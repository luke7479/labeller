package labeller;

public class Viewport {
	public int X;
	public int Y;
	public int WIDTH;
	public int HEIGHT;
	public double RATIO_X;
	public double RATIO_Y;
	public int IMAGE_X;
	public int IMAGE_Y;
	
	public Viewport(int x, int y, int width, int height, double ratioX, double ratioY){
		X = x;
		Y = y;
		WIDTH = width;
		HEIGHT = height;
		RATIO_X = ratioX;
		RATIO_Y = ratioY;
	}
	
	public String toString(){
		return "Viewport: X="+X+", Y="+Y+", RATIO_X="+RATIO_X+", RATIO_Y="+RATIO_Y;
	}
}
