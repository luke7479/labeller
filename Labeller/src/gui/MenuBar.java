package gui;

import java.awt.Event;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import labeller.Option;
import labeller.Registry;

public class MenuBar extends JMenuBar{
	private static final long serialVersionUID = 2090991217287327409L;
	private Actions actions;
	private String[] history = new String[4];
	private Map<JMenuItem, String> items = new HashMap<JMenuItem, String>();
	private Registry reg;
	private Option opt;
	
	public MenuBar(Actions actions, Registry reg, Option opt){
		this.actions = actions;
		this.reg = reg;
		this.opt = opt;
		
		history[0] = opt.get("history1");
		history[1] = opt.get("history2");
		history[2] = opt.get("history3");
		history[3] = opt.get("history4");
		
		menuProject();
		menuEdit();
		menuView();
		menuHelp();
	}
	
	private void menuHelp() {
		JMenu menu = new JMenu("Labeller");
		
		addItemtoMenu(menu,"Einstellungen",null,null,e->actions.settings());
		menu.addSeparator();
		addItemtoMenu(menu,"�ber",null,null,e->actions.about());
			
		this.add(menu);
		items.put(menu, null);
	}

	private void addItemtoMenu(JMenu menu,String name,String cat,KeyStroke key, ActionListener e){
		JMenuItem item;
		item = new JMenuItem(name);
		item.setAccelerator(key);
		item.addActionListener(e);
		menu.add(item);
		items.put(item, cat);
	}
	
	private void menuView() {
		JMenu menu = new JMenu("Ansicht");
		
		addItemtoMenu(menu,"Zoom +","project",KeyStroke.getKeyStroke(Integer.parseInt(opt.get("keyZoom+")),Integer.parseInt(opt.get("maskZoom+"))),e->actions.zoomIn());
		addItemtoMenu(menu,"Zoom -","project",KeyStroke.getKeyStroke(Integer.parseInt(opt.get("keyZoom-")),Integer.parseInt(opt.get("maskZoom-"))),e->actions.zoomOut());
		addItemtoMenu(menu,"Originalgr��e","project",KeyStroke.getKeyStroke(Integer.parseInt(opt.get("keyZoom100")),Integer.parseInt(opt.get("maskZoom100"))),e->actions.zoom(100));
		addItemtoMenu(menu,"Vollbild","project",KeyStroke.getKeyStroke(Integer.parseInt(opt.get("keyFullscreen")),Integer.parseInt(opt.get("maskFullscreen"))),e->actions.fullScreen());
			
		this.add(menu);
		items.put(menu, "project");
	}

	private void menuEdit() {
		JMenu menu = new JMenu("Bearbeiten");
		
		addItemtoMenu(menu,"R�ckg�ngig","project",KeyStroke.getKeyStroke(Integer.parseInt(opt.get("keyUndo")),Integer.parseInt(opt.get("maskUndo"))),e->actions.undo());
			
		this.add(menu);
		items.put(menu, "project");
	}

	private void menuProject(){
		JMenu menu = new JMenu("Projekt");

		addItemtoMenu(menu,"Neu","username",KeyStroke.getKeyStroke(Integer.parseInt(opt.get("keyNew")),Integer.parseInt(opt.get("maskNew"))),e->actions.newProject());
		addItemtoMenu(menu,"�ffnen",null,KeyStroke.getKeyStroke(Integer.parseInt(opt.get("keyOpen")),Integer.parseInt(opt.get("maskOpen"))),e->actions.load());
		addItemtoMenu(menu,"Schlie�en","project",KeyStroke.getKeyStroke(Integer.parseInt(opt.get("keyClose")),Integer.parseInt(opt.get("maskClose"))),e->actions.closeProject());
		addItemtoMenu(menu,"Speichern","modified",KeyStroke.getKeyStroke(Integer.parseInt(opt.get("keySave")),Integer.parseInt(opt.get("maskSave"))),e->actions.save());
		for(int i=0;i<history.length;i++){
			if(history[i] == null || history[i].isEmpty()) break;
			File filename = new File(history[i]);
			final String name = filename.getName().replaceAll("^[^_]*_", "").replaceAll("\\.lprj", "");
			addItemtoMenu(menu,name,null,KeyStroke.getKeyStroke(KeyEvent.VK_1+i,Event.CTRL_MASK),e->actions.load(filename.getAbsolutePath()));
		}
		menu.addSeparator();
		addItemtoMenu(menu,"Beenden",null,KeyStroke.getKeyStroke(Integer.parseInt(opt.get("keyQuit")),Integer.parseInt(opt.get("maskQuit"))),e->actions.close());
		
		this.add(menu);
	}
	
	public void repaint(){
		if(items == null || reg == null) return;
		
		for (Entry<JMenuItem, String> entry : items.entrySet()) {
			String key = entry.getValue();
			if(key == null) continue;
			JMenuItem item = entry.getKey();
		    
		    switch(key){
		    default:
		    	item.setEnabled(true);
		    	break;
		    case("project"):
		    	item.setEnabled(reg.project != null);
		    	break;
		    case("modified"):
		    	item.setEnabled(reg.modified);
		    	break;
		    case("undo"):
		    	item.setEnabled(reg.undo);
		    	break;
		    case("username"):
		    	item.setEnabled(!opt.get("username").isEmpty());
		    }
		}
	}
}
