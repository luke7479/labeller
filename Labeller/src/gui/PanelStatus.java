package gui;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;

public class PanelStatus extends JPanel{
	private static final long serialVersionUID = -2302747047488626442L;
	private JProgressBar status;
	private JProgressBar accuracy;
	private int[] progress = new int[]{0,0};
	private Double acc = 0.0;
	private Border etched = BorderFactory.createEtchedBorder();
	
	public PanelStatus(){
		setAlignmentX(0.0f);
		setAlignmentY(0.0f);
		setPreferredSize(new Dimension(150,getPreferredSize().height));
		setMaximumSize(new Dimension(150,getPreferredSize().height));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		accuracy = new JProgressBar(acc.intValue(),100);
		accuracy.setStringPainted(true);
		add(accuracy);
		
		//TODO Fortschrittsanzeige f�r up-/download
		
		status = new JProgressBar(progress[0], progress[1]);
		add(status);
		add(Box.createVerticalGlue());
	}

	public void repaint(){
		if(progress == null) return;
		status.setBorder(BorderFactory.createTitledBorder(etched, "Fortschritt: "+progress[0]+" / "+progress[1]));
		status.setMaximum(progress[1]);
		status.setValue(progress[0]);
		status.repaint();
		
		accuracy.setBorder(BorderFactory.createTitledBorder(etched, "Genauigkeit"));
		accuracy.setString(String.format("%.1f", acc)+" %");
		accuracy.setValue(acc.intValue());
		accuracy.repaint();
	}
	
	public void setMaximum(int max){
		if(max < 0) return;
		progress[1] = max;
	}
	public void setAcc(double acc){
		if(acc < 0.0 || acc > 100.0) return;
		this.acc = acc;
	}
}
