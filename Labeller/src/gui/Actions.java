package gui;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import server.Serverdata;
import labeller.Option;
import labeller.Project;
import labeller.Registry;

public class Actions {
	private Gui frame;
	private GraphicsDevice device;
	private Registry reg;
	private Option opt;
	private Serverdata server;
	private PanelView pView = null;
	private TBProject tbProject;
	
	public Actions(Gui frame, Registry reg, Option opt, Serverdata server){
		this.frame = frame;
		this.reg = reg;
		this.opt = opt;
		this.server = server;
		
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		device = ge.getDefaultScreenDevice();
	}
	
	public String openFile() throws IOException {
		JFileChooser chooser = new JFileChooser(new File(new File(".").getAbsolutePath()).getCanonicalPath()+"/projects");
	    FileNameExtensionFilter filter = new FileNameExtensionFilter("LPRJ Dateien", "lprj");
	    chooser.setFileFilter(filter);
	    int returnVal = chooser.showOpenDialog(chooser);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	       return chooser.getSelectedFile().getAbsolutePath();
	    }
	    return "";
	}
	
	public void close(){
		if(reg.modified){
			int result = JOptionPane.showConfirmDialog(null, "Vor dem Beenden speichern?", "Projekt speichern?", JOptionPane.YES_NO_CANCEL_OPTION);
			if(result == JOptionPane.CANCEL_OPTION) return;
			if (result == JOptionPane.YES_OPTION) save();
		}
		server.save();
		frame.dispose();
		System.exit(0);
	}
	
	public void closeProject(){
		if(server.loading() != 0){
			int result = JOptionPane.showConfirmDialog(null, "Es werden noch Daten vom/zum Server transferriert, Projekt wirklich schlie�en?", "Projekt schlie�en?", JOptionPane.YES_NO_CANCEL_OPTION);
			if(result == JOptionPane.CANCEL_OPTION) return;
			if (result == JOptionPane.YES_OPTION) save();
		}
		if(reg.modified){
			int result = JOptionPane.showConfirmDialog(null, "Vor dem Schließen speichern?", "Projekt speichern?", JOptionPane.YES_NO_CANCEL_OPTION);
			if(result == JOptionPane.CANCEL_OPTION) return;
			if (result == JOptionPane.YES_OPTION) save();
		}
		
		frame.close();
		frame.repaint();
	}
	
	public void fullScreen(){
		if(device.getFullScreenWindow() == frame)
			device.setFullScreenWindow(null);
		else if(device.isFullScreenSupported())
			device.setFullScreenWindow(frame);
			
	}
	
	public void load(){ load(""); }
	public void load(String filename) {
		try {
			if(filename.isEmpty()) filename = openFile();
		} catch (IOException e) {
			filename = "";
		}
		if(filename.isEmpty()) return;
		
		File file = new File(filename);
		if(!file.canRead()){
			JOptionPane.showMessageDialog(frame, "Datei konnte nicht gefunden/geöffnet werden!");
			return;
		}
		
		if(!file.getParent().endsWith("projects")){
			if(!new File("projects/"+file.getName()).exists()){
				file.renameTo(new File("projects/"+file.getName()));
				file = new File("projects/"+file.getName());
			}else{
				file.delete();
				file = new File("projects/"+file.getName());
			}
		}
		
		Project prj = new Project(file);
		if(!prj.loaded()){
			closeProject();
			return;
		}
		
		closeProject();
		reg.project = prj;
		pView.showImage(0);
		
		if(!filename.equals(opt.get("history1"))){
			opt.set("history4", opt.get("history3"));
			opt.set("history3", opt.get("history2"));
			opt.set("history2", opt.get("history1"));
			opt.set("history1", filename);
			opt.save();
		}
		
		tbProject.setLabels(reg.project.getLabelList());
		
		frame.repaint();
	}
	
	public boolean save(){
		if(!reg.modified) return true;
		//TODO: Save Project
		
		return true;
	}
	
	public void setPanelView(PanelView pView){
		this.pView = pView;
	}

	public boolean undo(){
		//TODO: Undo last step
		
		return true;
	}

	
	public void setTBProject(TBProject tbProject){
		this.tbProject = tbProject;
	}
	public void zoom(int i) {
		if(tbProject == null || pView == null) return;
		tbProject.setZoom(i);
		reg.zoom = i;
		frame.repaintImage(true, true);
	}
	public void zoomIn(){
		if(tbProject == null) return;
		zoom(tbProject.getNextZoom());
	}
	public void zoomOut(){
		if(tbProject == null) return;
		zoom(tbProject.getPrevZoom());
	}
	
	public void nextLabel(){
		tbProject.nextLabel();
	}
	public void prevLabel(){
		tbProject.prevLabel();
	}

	public void newProject() {
		new WizardProject(frame,server,opt);
	}

	public void about() {
		final JDialog dialog = new JDialog(frame, "Über", true);
		dialog.setSize(270, 180);
		dialog.setLocationRelativeTo(frame);
		dialog.setResizable(false);

	    Box b = Box.createVerticalBox();
	    b.add(Box.createGlue());
	    b.add(new JLabel(new ImageIcon(this.getClass().getResource(reg.resources+"Logo_gross.png"))));
	    b.add(new JLabel("© 2015 Sascha Haßler"));
	    b.add(new JLabel("Version: "+reg.version));
	    b.add(new JLabel(""));
	    b.add(Box.createGlue());
	    dialog.getContentPane().add(b, "Center");
	    
	    System.out.println("Test");

	    JPanel p2 = new JPanel();
	    JButton btnOK = new JButton("Ok");
	    p2.add(btnOK);
	    dialog.getContentPane().add(p2, "South");

	    btnOK.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent evt) {
	    	  dialog.setVisible(false);
	      }
	    });

	    dialog.setVisible(true);
		return;
	}

	public void settings() {
		new WizardOptions(frame,reg,opt,server);
		return;
	}
	
	public void nextImage(){
		nextImage(false);
	}
	public void nextImage(boolean skip){
		pView.nextImage();
	}
	public void prevImage(){
		prevImage(false);
	}
	public void prevImage(boolean skip){
		pView.prevImage();
	}
}
