package server;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import labeller.Option;
import labeller.Registry;

public class Serverdata {
	private Registry reg;
	private String downloadFile = "download.queue";
	private String uploadFile = "upload.queue";
	private ServerQueue<Downloadable> download = new ServerQueue<Downloadable>();
	private ServerQueue<Uploadable> upload = new ServerQueue<Uploadable>();
	private char loading = 0;
	private static Serverdata instance = null;
	
	public Serverdata(Registry reg, Option opt){
		if(instance != null) return;
		this.reg = reg;
		createWatchThread();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {}
		load();
		instance = this;
	}
	
	public static Serverdata getInstance(){
		return instance;
	}
	
	public void addDownload(Downloadable el){
		download.add(el);
	}
	public void addUpload(Uploadable el){
		upload.add(el);
	}
	
	public Downloadable getDownload(){
		return download.peek();
	}
	public Uploadable getUpload() {
		return upload.peek();
	}
	
	public Downloadable removeDownload(){
		return download.pop();
	}
	public Uploadable removeUpload(){
		return upload.pop();
	}
	
	public char loading(){
		return loading;
	}

	public boolean checkVersion(){
		if(!reg.online) return true;
		HttpURLConnection huc;
		huc = Server.connect("GET","action=version");
		try {
			InputStream content = huc.getInputStream();
			int response=-1;
			String code = "";
			while((response = content.read()) != -1){
				code += Character.toString((char)response);
			}
			content.close();
			
			if(code.equals(reg.version)) return true;
			return false;
		} catch (IOException e) {
			reg.online = false;
		}
		return false;
	}
	
	public void save(){
		download.save(downloadFile);
		upload.save(uploadFile);
	}
	
	public void load(){
		download.load(downloadFile);
		upload.load(uploadFile);
	}
	
	public boolean test(String username, String password){
		if(!reg.online) return false;
		
		HttpURLConnection huc = Server.connect("GET", "action=testAccount", username, password);
		int response=-1;
		String code = Server.getData(huc);
		response = Integer.parseInt(code);
		System.out.println(response);
		if(response < 0) return false;
		return true;
	}

	private void createWatchThread() {
		ThreadWatch temp = new ThreadWatch(reg);
		Thread watcher = new Thread(temp);
		watcher.start();
	}
	
	public void startDownload(){
		ThreadDownload temp = new ThreadDownload(this);
		Thread download = new Thread(temp);
		download.start();
	}

	public void startUpload(){
		ThreadUpload temp = new ThreadUpload(this);
		Thread upload = new Thread(temp);
		upload.start();
	}

	public int getUploads() {
		return upload.size();
	}

	public int getDownloads() {
		return download.size();
	}
}
