package gui;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import labeller.Viewport;


/**
 * Class to draw lines into graphics
 * @author Sascha Ha�ler
 * @version 1.0
 *
 */
public class DrawCross extends Drawable{
	private static final long serialVersionUID = -2618042777245701373L;
	private int x;
	private int y;
	private int width = 10;
	private int height = 10;
	private Color color;

	/**
	 * Constructor and setter for:
	 * @param x1 <b>[int]</b> X-position to start
	 * @param y1 <b>[int]</b> Y-position to start
	 * @param x2 <b>[int]</b> X-position to end
	 * @param y2 <b>[int]</b> Y-position to end
	 */
	public DrawCross(int x, int y){this(x,y,Color.BLACK);}
	public DrawCross(int x, int y, Color color){
		this.x=x;
		this.y=y;
		this.color = color;
	}
	
	public void setX(int x){ this.x = x; }
	public void setY(int y){ this.y = y; }
	
	public int getX(){ return this.x; }
	public int getY(){ return this.y; }
	public int[] getPos(){ return new int[]{x,y,width,height}; }
	
	/**
	 * Method to set line-color
	 * @param col <b>[Color]</b> {@link java.awt.Color}
	 */
	public void setColor(Color color){
		this.color = color;
	}	
	/**
	 * Returns parameters to String
	 */
	public String toString(){
		return "Cross: "+x+","+y;
	}

	@Override
	public void drawTo(Graphics g){
		drawTo(g, 100);
	}
	
	public void drawTo(Graphics g, int zoom){
		g.setColor(this.color);
		
		int x = (int)(this.x*zoom/100.0);
		int y = (int)(this.y*zoom/100.0);
		
		g.drawLine(x-width/2, y, x+width/2, y);
		g.drawLine(x, y-height/2, x, y+height/2);
	}
	@Override
	public void posToAbs(Viewport view, PanelView panelView, int zoom, BufferedImage resized) {
		x = Math.round((resized.getWidth() /2-view.X-panelView.getWidth() /2+x)*100.f/zoom);
		y = Math.round((resized.getHeight()/2-view.Y-panelView.getHeight()/2+y)*100.f/zoom);
	}
}
