package labeller;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageLoader{
	private Registry reg;
	private int previewIndex = -1;
	private BufferedImage image = null;
	private BufferedImage preview = null;
	private Viewport view;
	
	public ImageLoader(Registry reg){
		this.reg = reg;
	}

	public BufferedImage getImage(){
		if(reg.project == null) return null;
		
		String path = reg.project.getImage().toString();
		File file = new File(path);
		System.out.println(file.toString());
		if(!file.exists() || !file.canRead()) return null;
		try {
			image = ImageIO.read(file);
			return image;
		} catch (IOException e) {}
		return null;
	}
	
	public BufferedImage resizeImage(BufferedImage image, int newW, int newH){
		return resizeImage(image, newW, newH, null);
	}
	
	public BufferedImage resizeImage(BufferedImage image, int newW, int newH, ImageObserver obs){
		if(image == null) return null;
		
		Image tmp = image.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);
		
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp,0,0,obs);
		g2d.dispose();
		
		return resized;
	}

	public BufferedImage getPreview(int width, int height) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = image.createGraphics();
		
		g2d.setColor(new Color(80,80,80));
		g2d.fillRect(0, 0, width, height);
		if(this.image != null && previewIndex != reg.project.getCurrentImage()){
			previewIndex = reg.project.getCurrentImage();
			
			// Bildgr��e anpassen
			if(this.image.getWidth() != width && this.image.getHeight() != height){
				int newW = width;
				int newH = height;
				if(this.image.getHeight() >= this.image.getWidth()){
					newW = (int)((double)height/(double)this.image.getHeight()*(double)this.image.getWidth());
				}else{
					newH = (int)((double)width/(double)this.image.getWidth()*(double)this.image.getHeight());
				}
				preview = resizeImage(this.image,newW,newH,null);
			}else{
				preview = this.image;
			}
		}
		if(preview != null){
			// Bild mittig anzeigen
			int x = 0;
			int y = 0;
			if(preview.getWidth() < width) x = (width-preview.getWidth())/2;
			if(preview.getHeight() < height) y = (height-preview.getHeight())/2;
			g2d.drawImage(preview, x, y, null);

			BufferedImage border = new BufferedImage(preview.getWidth(), preview.getHeight(), BufferedImage.TYPE_INT_ARGB);
			g2d.drawImage(drawView(border),x,y,null);
		}
		
		g2d.dispose();
		return image;
	}
	
	private BufferedImage drawView(BufferedImage border){
		if(preview == null) return null;
		
		int width = (int)(view.WIDTH*(double)border.getWidth()/view.IMAGE_X);
		int height = (int)(view.HEIGHT*(double)border.getHeight()/view.IMAGE_Y);
		
		double ratioX = border.getWidth()/(double)view.IMAGE_X;
		double ratioY = border.getHeight()/(double)view.IMAGE_Y;
		int x = (int)((view.X*ratioX))-1;
		int y = (int)((view.Y*ratioY));
		
		if(width > border.getWidth()) width = border.getWidth();
		if(height > border.getHeight()) height = border.getHeight();
		
		Graphics2D g2d = border.createGraphics();
		
		g2d.setColor(Color.RED);
		g2d.drawRect((int)(border.getWidth()/2.0)-(int)(width/2.0)-x-1, (int)(border.getHeight()/2.0)-(int)(height/2.0)-y-1, width, height);
		g2d.drawRect((int)(border.getWidth()/2.0)-(int)(width/2.0)-x, (int)(border.getHeight()/2.0)-(int)(height/2.0)-y, width-2, height-2);
		g2d.dispose();
		
		return border;
	}

	public void close() {
		previewIndex = -1;
		image = null;
		preview = null;
	}

	public Position getPreviewSize() {
		if(preview == null) return null;
		return new Position(preview.getWidth(),preview.getHeight());
	}

	public void setView(Viewport view){
		this.view = view;
	}
}
