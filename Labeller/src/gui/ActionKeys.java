package gui;

import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;

import labeller.Option;

public class ActionKeys implements KeyEventDispatcher{
	Actions actions;
	private boolean locked = false;
	private Option opt = Option.getInstance();
	
	public ActionKeys(Actions actions) {
		this.actions = actions;
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent e) {
		if(locked) return false;
		if(e.getID() != KeyEvent.KEY_RELEASED) return false;
		
		if(e.getKeyCode() == Integer.parseInt(opt.get("keyNext1")) || e.getKeyCode() == Integer.parseInt(opt.get("keyNext2"))){
			locked = true;
			actions.nextImage();
		}
		if(e.getKeyCode() == Integer.parseInt(opt.get("keyForceNext"))){
			locked = true;
			actions.nextImage(true);
		}
		
		if(e.getKeyCode() == Integer.parseInt(opt.get("keyPrev"))){
			locked = true;
			actions.prevImage();
		}
		if(e.getKeyCode() == Integer.parseInt(opt.get("keyLabel"))){
			locked = true;
			if(e.isShiftDown()){
				actions.prevLabel();
			}else{
				actions.nextLabel();
			}
		}
		
		if(locked) e.consume();
		
		locked = false;
		return false;
	}
}
