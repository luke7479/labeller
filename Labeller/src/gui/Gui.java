package gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import server.Serverdata;
import labeller.ImageLoader;
import labeller.Option;
import labeller.Registry;

//Teste dies!

public class Gui extends JFrame{
	private static final long serialVersionUID = 8145033342383511163L;
	private Actions actions;
	private JMenuBar menuBar;
	private JToolBar tbMain;
	private JToolBar tbProject;
	private PanelStatus pStatus;
	private PanelView pView;
	private Container cPane;
	private Registry reg;
	private Option opt;
	private JPanel panels;
	private PanelPreview pPreview;
	private ImageLoader imageLoader;
	
	public Gui(Registry reg, Option opt, Serverdata server){
		this.reg = reg;
		this.opt = opt;
		
		createFrame();
		actions = new Actions(this,reg,opt,server);
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		ActionKeys keyactions = new ActionKeys(actions);
		manager.addKeyEventDispatcher( keyactions );
		
		addMenu();
		addToolBars();
		addPanel(server);
		
		
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		repaint();
	}
	
	private void addMenu(){
		menuBar = new MenuBar(actions,reg,opt);
		setJMenuBar(menuBar);
	}
	
	private void addPanel(Serverdata server){
		imageLoader = new ImageLoader(reg);
		pPreview = new PanelPreview(imageLoader);
		pStatus = new PanelStatus();
		pView = new PanelView(imageLoader, this, actions, reg, server);
		
		panels = new JPanel();
		panels.setLayout(new GridBagLayout());
		addGridPanel(pStatus, 1, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL);
		addGridPanel(pPreview, 1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE);
		addGridPanel(pView, 2, 1, 1, 2, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
		
		add(panels);
	}
	
	private void addGridPanel(JComponent panel, int gridX, int gridY, int gridWidth, int gridHeight, double weightX, double weightY, int anchor, int fill) {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = gridX;
		gbc.gridy = gridY;
		gbc.gridheight = gridHeight;
		gbc.gridwidth = gridWidth;
		gbc.weightx = weightX;
		gbc.weighty = weightY;
		gbc.anchor = anchor;
		gbc.fill = fill;
		panels.add(panel,gbc);
	}

	private void addToolBars() {
		JPanel panel = new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
	    
		tbMain = new TBMain(actions,reg);
		tbProject = new TBProject(actions,reg);
		panel.add(tbMain);
		panel.add(tbProject);
		cPane.add(panel, BorderLayout.PAGE_START);
	}

	private void createFrame(){
		ArrayList<Image> icons = new ArrayList<Image>();
		icons.add(new ImageIcon(this.getClass().getResource(reg.resources+"icon_16.png")).getImage());
		icons.add(new ImageIcon(this.getClass().getResource(reg.resources+"icon_24.png")).getImage());
		icons.add(new ImageIcon(this.getClass().getResource(reg.resources+"icon_32.png")).getImage());
		icons.add(new ImageIcon(this.getClass().getResource(reg.resources+"icon_64.png")).getImage());
		icons.add(new ImageIcon(this.getClass().getResource(reg.resources+"icon_128.png")).getImage());
		icons.add(new ImageIcon(this.getClass().getResource(reg.resources+"icon_256.png")).getImage());
		setIconImages(icons);
		
		cPane = getContentPane();
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				actions.close();
			}
		});
		setMinimumSize(new Dimension(600,450));
		setPreferredSize(new Dimension(800,600));
		setTitle("Labeller");
	}
	
	public void repaintImage(boolean view,boolean preview){
		if(view) pView.repaint();
		if(preview) pPreview.repaint();
	}
	
	public void repaint(){
		if(reg == null) return;
		tbMain.repaint();
		tbProject.repaint();
		menuBar.repaint();
		pStatus.repaint();
		repaintImage(true, true);
	}

	public void close() {
		reg.project = null;
		reg.modified = false;
		reg.zoom = 100;
		imageLoader.close();
	}
}



