package gui;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.event.MouseInputListener;

import labeller.Position;
import labeller.Registry;

public class ActionsView implements MouseWheelListener, MouseInputListener {
	private Actions actions;
	private int button = 0;
	private int startX = 0;
	private int startY = 0;
	private int scrollWidth = 20;
	private JFrame frame;
	private PanelView panel;
	private Registry reg;
	private int[] labelPos;
	
	public ActionsView(Actions actions, JFrame frame, PanelView panel, Registry reg) {
		this.actions = actions;
		this.frame = frame;
		this.panel = panel;
		this.reg = reg;
		this.labelPos = new int[]{0,0,0,0};
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if(e.getModifiers() == 2){ // CTRL
			if(e.getWheelRotation() > 0){ // runter > 0, hoch < 0
				actions.zoomOut();
			}else{
				actions.zoomIn();
			}
		}else if(e.getModifiers() == 1){ // Shift
			if(e.getWheelRotation() > 0){ // runter > 0, hoch < 0
				panel.scrollHorizontal(scrollWidth);
			}else{
				panel.scrollHorizontal(-scrollWidth);
			}
		}else{
			if(e.getWheelRotation() > 0){ // runter > 0, hoch < 0
				panel.scrollVertical(scrollWidth);
			}else{
				panel.scrollVertical(-scrollWidth);
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		panel.setMouseXY(new Position(-1,-1));
		if(button == 3){ // Rechte Maustaste
			int x = startX-e.getX();
			int y = startY-e.getY();
			panel.moveImage(x,y);
			startX = e.getX();
			startY = e.getY();
			return;
		}
		
		if(button == 1){
			labelPos[2] = e.getX();
			labelPos[3] = e.getY();
			panel.drawLabel(labelPos);
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if(button != 3) panel.setMouseXY(new Position(e.getX(),e.getY()));
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		button = e.getButton();
		startX = e.getX();
		startY = e.getY();
		if(button == 3) {
			frame.setCursor(new Cursor(Cursor.HAND_CURSOR));
			panel.setMouseXY(new Position(-1,-1));
			return;
		}
		if(button == 1){
			labelPos[0] = e.getX();
			labelPos[1] = e.getY();
			labelPos[2] = e.getX();
			labelPos[3] = e.getY();
			panel.drawLabel(labelPos);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(reg.project == null) return;
		frame.setCursor(java.awt.Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(1,1,BufferedImage.TYPE_4BYTE_ABGR),new java.awt.Point(0,0),"NOCURSOR"));
		panel.setMouseXY(new Position(e.getX(),e.getY()));
		panel.finishLabel();
		button = 0;
		startX = 0;
		startY = 0;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if(reg.project == null) return;
		frame.setCursor(java.awt.Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(1,1,BufferedImage.TYPE_4BYTE_ABGR),new java.awt.Point(0,0),"NOCURSOR"));
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if(reg.project == null) return;
		frame.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		panel.setMouseXY(new Position(-1,-1));
	}

}
