package labeller;

public class Position {
	public int X=0;
	public int Y=0;
	
	public Position(int x, int y) {
		set(x,y);
	}
	
	public void set(int x,int y){
		this.X = x;
		this.Y = y;
	}
}
