package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JToggleButton;

import labeller.Label;
import labeller.Registry;

public class TBProject extends ToolBar{
	private static final long serialVersionUID = -128085683008582292L;
	private JComboBox<Integer> cmbBox;
	private JComboBox<String> labelBox;
	private LinkedList<Label> labels = null;
	
	public TBProject(Actions actions, Registry reg){
		this.actions = actions;
		this.reg = reg;
		actions.setTBProject(this);
		addElements();
		addSeparator(new Dimension(8,20));
	}
	
	private void addElements(){
		JButton btn;
		JToggleButton tglBtn;
		
		btn = new JButton(new ImageIcon(this.getClass().getResource(reg.resources+"go-prev.png")));
			btn.setToolTipText("Zur�ck");
			buttons.put(btn, "project");
			btn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					actions.prevImage();
				}
			});
			add(btn);
		btn = new JButton(new ImageIcon(this.getClass().getResource(reg.resources+"go-next.png")));
			btn.setToolTipText("Weiter");
			buttons.put(btn, "project");
			btn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					actions.nextImage();
				}
			});
			add(btn);
		addSeparator(new Dimension(5,20));
		ButtonGroup group = new ButtonGroup();
		tglBtn = new JToggleButton(new ImageIcon(this.getClass().getResource(reg.resources+"zoomWidth.png")));
			tglBtn.setToolTipText("Breite anpassen");
			buttons.put(tglBtn, "project");
			tglBtn.setSelected(reg.zoom == 3);
			group.add(tglBtn);
			add(tglBtn);
		tglBtn = new JToggleButton(new ImageIcon(this.getClass().getResource(reg.resources+"zoomHeight.png")));
			tglBtn.setToolTipText("H�he anpassen");
			buttons.put(tglBtn, "project");
			tglBtn.setSelected(reg.zoom == 2);
			group.add(tglBtn);
			add(tglBtn);
		tglBtn = new JToggleButton(new ImageIcon(this.getClass().getResource(reg.resources+"zoomImage.png")));
			tglBtn.setToolTipText("Gesamtes Bild anzeigen");
			buttons.put(tglBtn, "project");
			tglBtn.setSelected(reg.zoom == 1);
			group.add(tglBtn);
			add(tglBtn);
		tglBtn = new JToggleButton(new ImageIcon(this.getClass().getResource(reg.resources+"zoomOriginal.png")));
			tglBtn.setToolTipText("Originalgr��e");
			buttons.put(tglBtn, "project");
			tglBtn.setSelected(reg.zoom == 0);
			group.add(tglBtn);
			add(tglBtn);
		cmbBox = new JComboBox<Integer>();
		cmbBox.setToolTipText("Zoomstufe");
		buttons.put(cmbBox, "project");
		cmbBox.setMaximumSize(new Dimension(60,24));
		int n=0;
		boolean selected = false;
		for(int i=50;i < 401;i+=25){
			cmbBox.addItem(i);
			
			if(reg.zoom == i){
				cmbBox.setSelectedIndex(n);
				selected = true;
			}
			
			if(i > 149) i+=25;
			n++;
		}
		if(!selected){
			int width = cmbBox.getWidth();
			cmbBox.setEditable(true);
			cmbBox.getEditor().setItem(reg.zoom);
			cmbBox.setPreferredSize(new Dimension(width,cmbBox.getHeight()));
		}
		cmbBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(cmbBox.getSelectedIndex() >= 0) cmbBox.setEditable(false);
				actions.zoom((int)cmbBox.getSelectedItem());
			}
		});
		add(cmbBox);
		addSeparator(new Dimension(5,20));
		
		labelBox = new JComboBox<String>();
		labelBox.setToolTipText("Labels");
		buttons.put(labelBox, "project");
		labelBox.setMaximumSize(new Dimension(100, 24));
		labelBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reg.labelIndex = labelBox.getSelectedIndex();
			}
		});
		add(labelBox);
		
		addSeparator(new Dimension(5,20));
	}
	
	public int getNextZoom(){
		int index=0;
		int ret = reg.zoom;
		if(cmbBox.isEditable()){
			ret = (int)cmbBox.getEditor().getItem();
			for(;index < cmbBox.getItemCount();index++){
				if(cmbBox.getItemAt(index) < ret) continue;
				if(cmbBox.getItemAt(index) >= ret) break;
			}
		}else{
			index = cmbBox.getSelectedIndex()+1;
		}
		if(index >= cmbBox.getItemCount()) index = cmbBox.getItemCount()-1; 
		return cmbBox.getItemAt(index);
	}
	public int getPrevZoom(){
		int index=0;
		int ret = reg.zoom;
		if(cmbBox.isEditable()){
			ret = (int)cmbBox.getEditor().getItem();
			for(;index < cmbBox.getItemCount();index++){
				if(cmbBox.getItemAt(index) < ret) continue;
				if(cmbBox.getItemAt(index) >= ret) break;
			}
			index--;
		}else{
			index = cmbBox.getSelectedIndex()-1;
		}
		if(index < 0) index = 0;
		return cmbBox.getItemAt(index);
	}
	public void setZoom(int percent){
		if(!cmbBox.isEnabled()) return;
		cmbBox.setSelectedItem(percent);
	}

	public void nextLabel() {
		if(labels == null || labels.size() < 1) return;
		if(labelBox.getSelectedIndex() == labelBox.getItemCount()-1){
			System.out.println("test");
			setLabel(0);
		}else{
			setLabel(labelBox.getSelectedIndex()+1);
		}
	}

	public void prevLabel() {
		if(labels == null || labels.size() < 1) return;
		if(labelBox.getSelectedIndex() == 0){
			setLabel(labelBox.getItemCount()-1);
		}else{
			setLabel(labelBox.getSelectedIndex()-1);
		}
	}
	
	public void setLabels(LinkedList<Label> labels){
		this.labels = labels;
		repaint();
	}
	
	public void setLabel(int label){
		if(labels == null || label >= labels.size() || label < 0) return;
		labelBox.setSelectedIndex(label);
		reg.labelIndex = label;
	}
	
	public Label getLabel(){
		if(labels == null || labels.size() < 1) return null;
		return labels.get(labelBox.getSelectedIndex());
	}
	
	@Override
	public void reDraw(){
		if(labelBox == null) return;
		labelBox.removeAllItems();
		if(labels != null && labels.size() > 0){
			for(int i=0;i < labels.size();i++){
				labelBox.addItem(labels.get(i).getName());
			}
		}
		labelBox.setEnabled(labelBox.getItemCount() > 0);
	}
}
