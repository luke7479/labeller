package labeller;

import java.io.Serializable;
import java.util.ArrayList;


public class OptionList implements Serializable{
	private static final long serialVersionUID = -3953941782310464725L;
	private ArrayList<String> key = new ArrayList<String>();
	private ArrayList<String> value = new ArrayList<String>();
	
	public String get(String name){
		for(int i=0;i < key.size();i++){
			if(name.equals(key.get(i))) return value.get(i);
		}
		return "";
	}
	
	public void set(String name, String val){
		for(int i=0;i < key.size();i++){
			if(name.equals(key.get(i))){
				value.set(i, val);
				return;
			}
		}
		key.add(name);
		value.add(val);
	}
	
	public boolean isSet(String name){
		for(int i=0;i < key.size();i++){
			if(name.equals(key.get(i))){
				return true;
			}
		}
		return false;
	}
}
