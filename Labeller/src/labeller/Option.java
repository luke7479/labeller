package labeller;

import java.awt.Event;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import server.Serverdata;

public class Option{
	private String filename = "options.bin";
	private OptionList opts;
	private Serverdata server;
	private Registry reg;
	private static Option instance;
	
	public Option(){
		opts = load(filename);
		if(opts == null) opts = new OptionList();
		
		if(!opts.isSet("keyNext1"))			opts.set("keyNext1", Integer.toString(KeyEvent.VK_SPACE));
		if(!opts.isSet("keyNext2"))			opts.set("keyNext2", Integer.toString(KeyEvent.VK_RIGHT));
		if(!opts.isSet("keyForceNext"))		opts.set("keyForceNext", Integer.toString(KeyEvent.VK_S));
		if(!opts.isSet("keyPrev"))			opts.set("keyPrev", Integer.toString(KeyEvent.VK_LEFT));
		if(!opts.isSet("keyLabel"))			opts.set("keyLabel", Integer.toString(KeyEvent.VK_TAB));

		if(!opts.isSet("keyZoom+"))			opts.set("keyZoom+", Integer.toString(KeyEvent.VK_PLUS));
		if(!opts.isSet("maskZoom+"))		opts.set("maskZoom+", Integer.toString(Event.CTRL_MASK));
		if(!opts.isSet("keyZoom-"))			opts.set("keyZoom-", Integer.toString(KeyEvent.VK_MINUS));
		if(!opts.isSet("maskZoom-"))		opts.set("maskZoom-", Integer.toString(Event.CTRL_MASK));
		if(!opts.isSet("keyZoom100"))		opts.set("keyZoom100", Integer.toString(KeyEvent.VK_0));
		if(!opts.isSet("maskZoom100"))		opts.set("maskZoom100", Integer.toString(Event.CTRL_MASK));
		if(!opts.isSet("keyFullscreen"))	opts.set("keyFullscreen", Integer.toString(KeyEvent.VK_F11));
		if(!opts.isSet("maskFullscreen"))	opts.set("maskFullscreen", Integer.toString(0));

		if(!opts.isSet("keyNew"))			opts.set("keyNew", Integer.toString(KeyEvent.VK_N));
		if(!opts.isSet("maskNew"))			opts.set("maskNew", Integer.toString(Event.CTRL_MASK));
		if(!opts.isSet("keyOpen"))			opts.set("keyOpen", Integer.toString(KeyEvent.VK_O));
		if(!opts.isSet("maskOpen"))			opts.set("maskOpen", Integer.toString(Event.CTRL_MASK));
		if(!opts.isSet("keyClose"))			opts.set("keyClose", Integer.toString(KeyEvent.VK_W));
		if(!opts.isSet("maskClose"))		opts.set("maskClose", Integer.toString(Event.CTRL_MASK));
		if(!opts.isSet("keySave"))			opts.set("keySave", Integer.toString(KeyEvent.VK_S));
		if(!opts.isSet("maskSave"))			opts.set("maskSave", Integer.toString(Event.CTRL_MASK));
		if(!opts.isSet("keyQuit"))			opts.set("keyQuit", Integer.toString(KeyEvent.VK_Q));
		if(!opts.isSet("maskQuit"))			opts.set("maskQuit", Integer.toString(Event.CTRL_MASK));

		if(!opts.isSet("keyUndo"))			opts.set("keyUndo", Integer.toString(KeyEvent.VK_Z));
		if(!opts.isSet("maskUndo"))			opts.set("maskUndo", Integer.toString(Event.CTRL_MASK));

		if(!opts.isSet("language"))			opts.set("language", "deDE");
	}
	
	public static Option getInstance (Registry reg){
		if(instance == null) instance = new Option();
		instance.reg = reg;
		return instance;
	}
	public static Option getInstance(){
		return instance;
	}

	public void setServer(Serverdata server){
		this.server = server;
	}
	
	public OptionList getOptionList(){
		return opts;
	}
	public String getFilename(){
		return filename;
	}
	
	public OptionList load(String filename){
		File file = new File(filename);
		if(!file.exists() || !file.canRead()) return null;
		
		ObjectInputStream in = null;
		
		try {
			in = new ObjectInputStream(new FileInputStream(file));
			return (OptionList)in.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Fehlerdialog
		}finally{
			try {
				if(in != null) in.close();
			} catch (IOException e) {
				//TODO Fehlerdialog
			}
		}
		return null;
	}
	
	private void validateData(){
		if(reg.online && !server.test(get("username"), get("password"))){
			set("username","");
			set("password","");
		}
	}
	
	public void save(){
		save(filename,opts);
	}
	public void save(String filename, OptionList opts){
		File file = new File(filename);
		if(file.exists() && !file.canWrite()) return;
		
		validateData();
		
		ObjectOutputStream out = null;
		
		try {
			out = new ObjectOutputStream(new FileOutputStream(file));
			out.writeObject(opts);
		} catch (IOException e) {
			// TODO Fehlerdialog
		}finally{
			try {
				if(out != null) out.close();
			} catch (IOException e) {
				//TODO Fehlerdialog
			}
		}
	}
	
	public String get(String name){ return opts.get(name); }
	public void set(String name, String val){ opts.set(name,val); }

	public void setOptionList(OptionList opts) {
		if(opts == null) return;
		this.opts = opts;
	}
}
