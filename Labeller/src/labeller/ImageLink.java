package labeller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import server.Downloadable;
import server.Uploadable;

public class ImageLink implements Uploadable, Downloadable, Serializable {
	private static final long serialVersionUID = 5710577914764442849L;
	private String link;
	private String project = null;
	private boolean pathCleaned = false;

	public ImageLink(String link){
		this.link = link;
	}
	
	public ImageLink(ImageLink imageLink) {
		link = imageLink.getLink();
		project = imageLink.getProject();
	}

	private String getLink() {
		return link;
	}

	public String toString(){
		return link;
	}
	
	public String getProject(){
		return project;
	}
	public void setProject(String project){
		this.project = project;
	}
	
	public boolean pathCleaned(){
		return pathCleaned;
	}

	@Override
	public byte[] toByteArray() {
		File file = new File(link);
		if(file == null || !file.exists() || !file.canRead()) return new byte[]{};


		ByteArrayOutputStream bos = null;
		ImageOutputStream ios = null;
		byte[] data;
		try {
			ImageWriter jpgWriter = ImageIO.getImageWritersByFormatName("jpg").next();
			ImageWriteParam jpgWriteParam = jpgWriter.getDefaultWriteParam();
			jpgWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			jpgWriteParam.setCompressionQuality(.3f);
			bos = new ByteArrayOutputStream();
			ios = ImageIO.createImageOutputStream(bos);
			jpgWriter.setOutput(ios);
			IIOImage outputImage = new IIOImage(ImageIO.read(file), null, null);
			jpgWriter.write(null, outputImage, jpgWriteParam);
			jpgWriter.dispose();
			data = bos.toByteArray();
		} catch (IOException e) {
			data = new byte[]{};
		} finally {
			try {
				if(ios != null) ios.close();
				if(bos != null) bos.close();
			} catch (IOException e) {}
		}
		
		return data;
	}

	public void deletePath(String project) {
		if(pathCleaned) return;
		link = "projects/"+project+"/"+fileName();
		pathCleaned = true;
	}

	public String fileName() {
		File file = new File(link);
		String filename = file.getName().substring(0, file.getName().lastIndexOf('.'));
		filename = filename.replaceAll(" ", "_");
		filename = filename.replaceAll("�", "ae");
		filename = filename.replaceAll("�", "oe");
		filename = filename.replaceAll("�", "ue");
		filename = filename.replaceAll("�", "Ae");
		filename = filename.replaceAll("�", "Oe");
		filename = filename.replaceAll("�", "Ue");
		filename = filename.replaceAll("�", "ss");
		return filename+".jpg";
	}
}
