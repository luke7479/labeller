package labeller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Serverdata {
	private String link = "http://labellerTool.it-hassler.selfhost.eu/labeller.php";
	private Registry reg;
	private Option opt;
	private ServerQueue<Loadable> download = new ServerQueue<Loadable>();
	private ServerQueue<Loadable> upload = new ServerQueue<Loadable>();
	
	public Serverdata(Registry reg, Option opt){
		this.reg = reg;
		this.opt = opt;
		createWatchThread();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {}
	}
	
	public void addDownload(Loadable el){
		download.add(el);
	}
	public void addUpload(Loadable el){
		upload.add(el);
	}
	
	public boolean checkFile(String filename){
		return(checkFile(filename,filename));
	}
	public boolean checkFile(String filename, String target){
		File file = new File(filename);
		if(!file.exists() || !file.canRead()) return false;
		
		HttpURLConnection huc = connect("POST", "filename="+target);
		
		String s = getData(huc);
		if(!s.equals(md5_file(filename))) return false;
		
		return true;
	}
	
	private HttpURLConnection connect(String type, String param) {
		HttpURLConnection huc = null;
		try {
			String target = link+"?action=checkFile&username="+opt.get("username")+"&password="+opt.get("password")+"&"+param;
			URL url = new URL(target);
			huc = (HttpURLConnection)url.openConnection();

			if(type.equals("POST")){
				huc.setDoOutput(true);
				huc.setRequestMethod("POST");
			}else if(type.equals("GET")){
				huc.setRequestMethod("GET");
			}
			huc.connect();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return huc;
	}
	
	private String getData(HttpURLConnection huc){
		InputStreamReader isr = null;
		BufferedReader in = null;
		String s = "";
		try {
			isr = new InputStreamReader(huc.getInputStream());
			in = new BufferedReader(isr);
			
			s = in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if(in != null) in.close();
				if(isr != null) isr.close();
			} catch (IOException e) {}
		}
		
		return s;
	}

	public boolean checkVersion(){
		if(!reg.online) return true;
		HttpURLConnection huc;
		try {
			URL url = new URL(link+"?action=version");
			huc = ( HttpURLConnection )  url.openConnection ();
			huc.setRequestMethod ("GET");  //OR  huc.setRequestMethod ("HEAD"); 
			huc.connect () ; 
			InputStream content = huc.getInputStream();
			int response=-1;
			String code = "";
			while((response = content.read()) != -1){
				code += Character.toString((char)response);
			}
			content.close();
			if(code.equals(reg.version)) return true;
			return false;
		} catch (IOException e) {
			reg.online = false;
		}
		return false;
	}
	
	public boolean save(){
		File downloadFile = new File("download.queue");
		File uploadFile = new File("upload.queue");
		
		if(download.size() > 0){
			FileOutputStream fos=null;
			ObjectOutputStream out=null;
			try {
				fos = new FileOutputStream(downloadFile);
				out = new ObjectOutputStream(fos);
				out.writeObject(download);
			} catch (IOException e) {
				return false;
			} finally{
				try {
					fos.close();
					out.close();
				} catch (Exception e1) {}
			}
		}
		if(upload.size() > 0){
			FileOutputStream fos=null;
			ObjectOutputStream out=null;
			try {
				fos = new FileOutputStream(uploadFile);
				out = new ObjectOutputStream(fos);
				out.writeObject(upload);
			} catch (IOException e) {
				return false;
			} finally{
				try {
					fos.close();
					out.close();
				} catch (Exception e1) {}
			}
		}
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public void load(){
		File downloadFile = new File("download.queue");
		File uploadFile = new File("upload.queue");
		
		if(downloadFile.exists() && downloadFile.canRead()){
			FileInputStream fis=null;
			ObjectInputStream in=null;
			try {
				fis = new FileInputStream(downloadFile);
				in = new ObjectInputStream(fis);
				download = (ServerQueue<Loadable>) in.readObject();
			} catch (IOException | ClassNotFoundException e) {
				return;
			} finally{
				try {
					fis.close();
					in.close();
				} catch (Exception e1) {}
			}
		}
		if(uploadFile.exists() && uploadFile.canRead()){
			FileInputStream fis=null;
			ObjectInputStream in=null;
			try {
				fis = new FileInputStream(uploadFile);
				in = new ObjectInputStream(fis);
				upload = (ServerQueue<Loadable>) in.readObject();
			} catch (IOException | ClassNotFoundException e) {
				return;
			} finally{
				try {
					fis.close();
					in.close();
				} catch (Exception e1) {}
			}
		}
	}
	
	public boolean test(String username, String password){
		if(!reg.online) return false;
		
		HttpURLConnection huc;
		try {
			URL url = new URL(link+"?action=test&username="+username+"&password="+password);
			huc = ( HttpURLConnection )  url.openConnection ();
			huc.setRequestMethod ("GET");  //OR  huc.setRequestMethod ("HEAD"); 
			huc.connect () ; 
			InputStream content = huc.getInputStream();
			int response=-1;
			String code = "";
			while((response = content.read()) != -1){
				code += Character.toString((char)response);
			}
			response = Integer.parseInt(code);
			content.close();
			if(response < 0) return false;
			return true;
		} catch (IOException e) {
			reg.online = false;
		}
		return false;
	}

	private void createWatchThread() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Thread.currentThread().setName("ConnectionCheck");
				HttpURLConnection huc;
				while(true){
					try {
						huc = ( HttpURLConnection )  new URL(link).openConnection ();
						huc.setReadTimeout(100);
						huc.setConnectTimeout(400);
						int code = huc.getResponseCode() ;
						if(code > 0) reg.online = true;
						else reg.online = false;
						Thread.sleep(30000);
					} catch (Exception e) {
						reg.online = false;
					}
				}
			}
		}).start();
	}
	
	private String md5_file(String filename){
		File file = new File(filename);
		if(!file.exists() || !file.canRead()) return "";
		
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
            File f=new File(filename);
            FileInputStream is=new FileInputStream(f);
            byte[] buffer=new byte[(int)file.length()];
            int read=0;
            while((read = is.read(buffer)) > 0){
            	md.update(buffer, 0, read);
            }
            is.close();
            return new BigInteger(1, md.digest()).toString(16);
		} catch (NoSuchAlgorithmException | IOException e) {}
		
		return "";
	}
}
