package server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import labeller.ImageLink;
import labeller.Option;

public class Server {
	private static String link = "http://labellerTool.it-hassler.selfhost.eu/labeller.php";
	private static Option opt = Option.getInstance();
	private static boolean online = false;
	
	public static boolean test(){
		HttpURLConnection huc = null;
		int code = 0;
		try {
			huc = ( HttpURLConnection )  new URL(link).openConnection ();
			huc.setReadTimeout(100);
			huc.setConnectTimeout(400);
			code = huc.getResponseCode();

			online = code > 0;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return online;
	}
	
	public static HttpURLConnection connect(String type, String param){
		return connect(type, param, opt.get("username"), opt.get("password"));
	}
	public static HttpURLConnection connect(String type, String param, String username, String password) {
		if(!online) return null;
		
		HttpURLConnection huc = null;
		try {
			String target = link+"?username="+username+"&password="+password+"&"+param;
			System.out.println(target);
			URL url = new URL(target);
			huc = (HttpURLConnection)url.openConnection();

			if(type.equals("POST")){
				huc.setDoOutput(true);
				huc.setRequestMethod("POST");
			}else if(type.equals("GET")){
				huc.setRequestMethod("GET");
			}
			huc.connect();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return huc;
	}
	
	public static boolean online(){
		return online;
	}
	
	public static boolean checkFile(ImageLink imagelink){
		return checkFile(new File(imagelink.toString()), imagelink.getProject()+"/"+imagelink.fileName());
	}
	public static boolean checkFile(File file){
		return checkFile(file, file.getName());
	}
	public static boolean checkFile(File local, String remote){
		if(!local.exists() || !local.canRead()) return false;
		
		HttpURLConnection huc = Server.connect("POST", "action=checkFile&filename="+remote);
		
		String s = getData(huc);
		return s.equals(md5_file(local));
	}
	
	public static String getData(HttpURLConnection huc){
		InputStreamReader isr = null;
		BufferedReader in = null;
		String s = "";
		try {
			isr = new InputStreamReader(huc.getInputStream());
			in = new BufferedReader(isr);
			
			s = in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if(in != null) in.close();
				if(isr != null) isr.close();
			} catch (IOException e) {}
		}
		
		return s;
	}
	
	public static String md5_file(File file){
		if(!file.exists() || !file.canRead()) return "";
		
		FileInputStream is = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			is=new FileInputStream(file);
            byte[] buffer=new byte[(int)file.length()];
            int read=0;
            while((read = is.read(buffer)) > 0){
            	md.update(buffer, 0, read);
            }
            return (new HexBinaryAdapter()).marshal(md.digest()).toLowerCase();
		} catch (NoSuchAlgorithmException | IOException e) {
		} finally {
			try {
				if(is != null) is.close();
			} catch (IOException e) {}
		}
		
		return "";
	}

	public static String getLink() {
		return link;
	}
}
