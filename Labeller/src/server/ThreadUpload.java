package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import labeller.ImageLink;
import labeller.Label;
import labeller.Project;

public class ThreadUpload implements Runnable {
	private Serverdata data;

	public ThreadUpload(Serverdata data){
		this.data = data;
	}

	@Override
	public void run() {
		Thread.currentThread().setName("Upload");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {}
		while(true){
			if(!Server.online() || data.getUploads() < 1){
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {}
				continue;
			}
			
			Uploadable upload = data.getUpload();
			if(upload == null) continue;
			upload(upload, 1);
		}
	}
	
	private void upload(Uploadable upload, int tries) {
		byte[] send;
		HttpURLConnection huc = null;
		if(upload instanceof Label){
			Label label = (Label)upload;
			huc = Server.connect("POST","action=sendLabel&project="+label.getProject());
			send = label.toByteArray();
		}else if(upload instanceof ImageLink){
			ImageLink img = (ImageLink)upload;
			String update = (tries > 1)? "&update=1" : "";
			huc = Server.connect("POST","action=sendImage&filename="+img.fileName()+"&project="+img.getProject()+update);
			send = img.toByteArray();
		}else if(upload instanceof Project){
			Project project = (Project)upload;
			huc = Server.connect("POST","action=sendProject&filename="+project.getFilename());
			project.deletePaths();
			send = project.toByteArray();
		}else{
			return;
		}
		
		String md5 = sendData(send, huc);
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(send);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String checkMD5 = (new HexBinaryAdapter()).marshal(md.digest()).toLowerCase();
        if(md != null && checkMD5.equals(md5)){
        	data.removeUpload();
        }else{
        	try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {}
        	upload(upload, tries+1);
        }
	}

	public void start(){
		this.start();
	}
	
	private String sendData(byte[] data, HttpURLConnection huc){
		if(data == null || data.length < 1 || huc == null) return "";
		
		OutputStream out = null;
		BufferedReader in = null;
		try {
			out = huc.getOutputStream();
			out.write(data);
			
			in = new BufferedReader(
				new InputStreamReader(huc.getInputStream())
			);
			return in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(in != null) in.close();
				if(out != null) out.close();
			} catch (IOException e) {}
		}

		return "";
	}
}
