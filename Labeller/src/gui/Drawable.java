package gui;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.Serializable;

import labeller.Viewport;

public abstract class Drawable implements Serializable{
	private static final long serialVersionUID = -4480121944220540124L;


	public Drawable(){
	}
	
	
	public void drawTo(Graphics graphics){
		System.out.println("not implemented");
	}
	public void drawTo(Graphics graphics, int zoom){
		System.out.println("Zoom not implemented");
	}


	public abstract void posToAbs(Viewport view, PanelView panelView, int zoom, BufferedImage resized);
	
	public abstract int[] getPos();
}
