package server;

import labeller.Registry;

public class ThreadWatch implements Runnable {
	Registry reg = null;
	
	public ThreadWatch(Registry reg){
		this.reg = reg;
	}
	
	@Override
	public void run() {
		Thread.currentThread().setName("ConnectionCheck");
		while(true){
			reg.online = Server.test();
			try {
				Thread.sleep(30000);
			} catch (Exception e) {}
		}
	}

//	public void start() {
//		super.start();
//	}
}
