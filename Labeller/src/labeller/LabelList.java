package labeller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class LabelList implements Serializable{
	private static final long serialVersionUID = -4931029857955813215L;
	private HashMap<Integer, List<Label>> labels;
	
	public LabelList(){
		labels = new HashMap<Integer, List<Label>>();
	}
	
	public void add(int nImage, Label label){
		List<Label> cur = getLabels(nImage);
		cur.add(label);
		labels.put(nImage, cur);
	}
	
	public List<Label> getLabels(int nImage){
		if(labels.containsKey(nImage))
			return new LinkedList<Label>(labels.get(nImage));
		return new LinkedList<Label>();
	}
	
	public void remove(int nImage, Label label){
		List<Label> cur = getLabels(nImage);
		cur.remove(label);
	}
	
	public int size(int image){
		return getLabels(image).size();
	}
}
