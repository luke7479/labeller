package gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import server.Serverdata;
import labeller.Option;
import labeller.Registry;

public class WizardOptions extends JDialog{
	private static final long serialVersionUID = 1L;
	private Option opt;
	private Container pane;
	private Serverdata server;
	private Registry reg;
	private JFrame frame;
	
	public WizardOptions(JFrame parent, Registry reg, Option opt, Serverdata server){
		super(parent,"Einstellungen",Dialog.ModalityType.DOCUMENT_MODAL);
		this.opt = opt;
		this.server = server;
		this.reg = reg;
		this.frame = parent;
		pane = getContentPane();
		
		setLocationRelativeTo(parent);
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(400,200));
		
		
		pack();
		addTabs();
		addButtons();
		
		
		setVisible(true);
	}
	
	private void addTabs(){
		JTabbedPane tabs = new JTabbedPane(JTabbedPane.LEFT);
//		tabs.setTabPlacement(JTabbedPane.LEFT);
		
		panelLogin(tabs);
		
		pane.add(tabs,BorderLayout.PAGE_START);
	}
	
	private void panelLogin(JTabbedPane tabs) {
		JPanel panelLogin = new JPanel();
		final JButton btnTest = new JButton("Testen");
		
		panelLogin.setLayout(new GridBagLayout());
		
		addGrid(panelLogin,1,1,1.0,1.0,1,1,new JLabel("Benutzername: "));
		final JTextField txtUser = new JTextField(opt.get("username"),20);
		txtUser.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) { activate(); }
			public void removeUpdate(DocumentEvent e) { activate(); }
			public void insertUpdate(DocumentEvent e) { activate(); }
			public void activate() {
				btnTest.setEnabled(true);
			}
		});
		addGrid(panelLogin,2,1,1.0,1.0,1,1,txtUser);
		
		addGrid(panelLogin,1,2,1.0,1.0,1,1,new JLabel("Passwort: "));
		final JPasswordField txtPass = new JPasswordField(opt.get("password"));
		txtPass.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) { activate(); }
			public void removeUpdate(DocumentEvent e) { activate(); }
			public void insertUpdate(DocumentEvent e) { activate(); }
			public void activate() {
				btnTest.setEnabled(true);
			}
		});
		addGrid(panelLogin,2,2,1.0,1.0,1,1,txtPass);
		
		btnTest.setEnabled(false);
		btnTest.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!reg.online){
					JOptionPane.showMessageDialog(frame, "Keine Verbindung m�glich");
					return;
				}
				String pass = "";
				try {
					MessageDigest md;
					md = MessageDigest.getInstance("SHA-256");
					md.update(new String(txtPass.getPassword()).getBytes("UTF-8")); // Change this to "UTF-16" if needed
					pass = new BigInteger(1, md.digest()).toString(16);
				} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {}

				if(!server.test(txtUser.getText(), pass)){
					JOptionPane.showMessageDialog(frame, "Benutzername/Passwort falsch");
				}else{
					opt.set("username", txtUser.getText());
					opt.set("password", new String(pass));
					JOptionPane.showMessageDialog(frame, "Verbindung erfolgreich");
				}
			}
		});
		addGrid(panelLogin,1,3,1.0,1.0,2,1,btnTest);
		
		tabs.addTab("Login", panelLogin);
	}

	private void addGrid(JComponent into, int gridX, int gridY, double weightX, double weightY, int width, int height, JComponent what) {
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = gridX;
		c.gridy = gridY;
		c.weightx = weightX;
		c.weighty = weightY;
		c.gridwidth = width;
		c.gridheight = height;
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.HORIZONTAL;
		into.add(what, c);
	}

	private void addButtons(){
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		JButton btnCancel = new JButton("Abbrechen");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				opt.setOptionList(opt.load(opt.getFilename()));
				WizardOptions.this.dispatchEvent(
					new WindowEvent(WizardOptions.this, WindowEvent.WINDOW_CLOSING)
				);
			}
		});
		panel.add(btnCancel);
		
		JButton btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				opt.save(opt.getFilename(),opt.getOptionList());
				frame.repaint();
				WizardOptions.this.dispatchEvent(
					new WindowEvent(WizardOptions.this, WindowEvent.WINDOW_CLOSING)
				);
			}
		});
		panel.add(btnOK);
		
		add(panel,BorderLayout.PAGE_END);
	}
}
