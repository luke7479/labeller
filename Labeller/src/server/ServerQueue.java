package server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;


public class ServerQueue<T>{
	private LinkedList<T> queue = new LinkedList<T>();
	private int version = 1;
	
	public void add(T e){
		if(queue.contains(e)) return;
		queue.add(e);
	}
	
	public T peek(){
		if(queue.size() < 1) return null;
		return queue.getFirst();
	}
	
	public T pop(){
		if(size() < 1) return null;
		
		return queue.removeFirst();
	}
	
	public int size(){
		return queue.size();
	}
	
	@SuppressWarnings("unchecked")
	public void load(String filename){
		File file = new File(filename);
		if(!file.exists() || !file.canRead()) return;
		
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(file);
			in = new ObjectInputStream(fis);
			int fileVersion = in.readInt();
			switch(fileVersion){
			case(1):
				queue = (LinkedList<T>)in.readObject();
			}
		} catch (IOException | ClassNotFoundException e) {
			queue = new LinkedList<T>();
			e.printStackTrace();
		}finally{
			try {
				if(in != null) in.close();
				if(fis != null) fis.close();
			} catch (IOException e) {}
		}
	}
	
	public void save(String filename){
		File file = new File(filename);
		if(queue.size() < 1){
			if(file.exists()) file.delete();
			return;
		}
		try {
			if(!file.exists()) file.createNewFile();
		} catch (IOException e) {
			return;
		}
		if(!file.canWrite()) return;
		
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try {
			fos = new FileOutputStream(file);
			out = new ObjectOutputStream(fos);
			out.writeInt(version);
			out.writeObject(queue);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if(out != null) out.close();
				if(fos != null) fos.close();
			} catch (IOException e) {}
		}
	}
}
