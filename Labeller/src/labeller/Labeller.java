package labeller;

import gui.Gui;

import javax.swing.JOptionPane;

import server.Serverdata;

public class Labeller {
	private Registry reg;
	private Option opt;
	private Serverdata server;

	public Labeller(){
		this.reg = new Registry();
		this.opt = Option.getInstance(reg);
		this.server = new Serverdata(reg, opt);
		opt.setServer(server);
		if(!server.checkVersion()){
			int result = JOptionPane.showConfirmDialog(null, "Willst du diese alte Version benutzen?", "Update verf�gbar!", JOptionPane.YES_NO_CANCEL_OPTION);
			if(result == JOptionPane.CANCEL_OPTION || result == JOptionPane.NO_OPTION) System.exit(0);
		}

		server.load();
		server.startUpload();
		server.startDownload();
		new Gui(reg,opt,server);
	}

	public static void main(String[] args) {
		new Labeller();
	}
}
