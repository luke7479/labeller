package gui;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JComponent;
import javax.swing.JToolBar;

import labeller.Registry;



public class ToolBar extends JToolBar {
	private static final long serialVersionUID = 2320988319363068288L;
	protected Map<JComponent, String> buttons = new HashMap<JComponent, String>();
	protected Actions actions;
	protected Registry reg;
	
	public ToolBar() {
		setFloatable(false);
	}
	
	
	@SuppressWarnings("unused")
	private void addElements(){}
	
	public void repaint(){
		if(buttons == null) return;
		
		for (Entry<JComponent, String> entry : buttons.entrySet()) {
			String key = entry.getValue();
			if(key == null) continue;
			JComponent btn = entry.getKey();
		    
		    switch(key){
		    default:
		    	btn.setEnabled(true);
		    	break;
		    case("project"):
		    	btn.setEnabled(reg.project != null);
		    	break;
		    case("modified"):
		    	btn.setEnabled(reg.modified);
		    	break;
		    }
		}
		
		reDraw();
	}


	public void reDraw(){}
}
