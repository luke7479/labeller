package gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import labeller.ImageLoader;

public class PanelPreview extends JPanel{
	int width = 120;
	int height = 120;
	int border = 30;
	private static final long serialVersionUID = -7467621976365057160L;
	ImageLoader loader;
	
	public PanelPreview(ImageLoader loader){
		this.loader = loader;
		setPreferredSize(new Dimension(width+border, height+border));
	}
	
	@Override
	public void paintComponent(Graphics g){
		int x = border/2;
		int y = border/2;
		BufferedImage buf = new BufferedImage(width+border,height+border,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = buf.createGraphics();
		g2d.setColor(getBackground());
		g2d.fillRect(0, 0, getWidth(), getHeight());
		g2d.drawImage(loader.getPreview(width, height), x, y, null);
		g2d.dispose();
		g.drawImage(buf, 0, 0, null);
	}
}
