package labeller;

import gui.DrawCircle;
import gui.DrawCross;
import gui.DrawLine;
import gui.DrawRect;
import gui.Drawable;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.Icon;

import server.Uploadable;

public class Label implements Serializable, Uploadable{
	private static final long serialVersionUID = -7017892537303170747L;
	private int amount;
	private Color color;
	private String name;
	private LabelIcon icon;
	private String form;
	private Drawable label;
	private boolean uploaded = false;
	private String project = "";
	
	public Label(){
		this("Label",0,Color.BLACK,"Punkt");
	}
	public Label(String name, int amount, Color color, String form) {
		this.name = name;
		this.amount = amount;
		this.color = color;
		this.icon = new LabelIcon();
		this.form = form;
	}
	
	public Label(Label src) {
		this.amount = src.getAmount();
		this.color	= src.getColor();
		this.name	= src.getName();
		this.icon	= src.getIcon();
		this.form	= src.getForm();
		this.label	= src.getLabel();
	}
	public int getAmount(){
		return amount;
	}
	public Color getColor(){
		return color;
	}
	public String getForm(){
		return form;
	}
	public String getName(){
		return name;
	}
	public LabelIcon getIcon(){
		return icon;
	}
	public Drawable getLabel(){
		return label;
	}
	public String getProject(){
		return project;
	}
	public boolean getUplaoded(){
		return uploaded;
	}
	
	public void setAmount(int amount){
		this.amount = amount;
	}
	public void setColor(Color color){
		this.color = color;
	}
	public void setForm(String form){
		this.form = form;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setLabel(Drawable label){
		if(this.form == "Kreis" && !(label instanceof DrawCircle)) return;
		if(this.form == "Linie" && !(label instanceof DrawLine)) return;
		if(this.form == "Punkt" && !(label instanceof DrawCross)) return;
		if(this.form == "Rechteck" && !(label instanceof DrawRect)) return;
		
		this.label = label;
	}
	public void setProject(String name){
		project = name;
	}
	public void setUplaoded(boolean uploaded){
		this.uploaded = uploaded;
	}
	
	@Override
	public String toString(){
		return " "+Integer.toString(amount)+" "+name;
	}

	public class LabelIcon implements Icon, Serializable{
		private static final long serialVersionUID = 1L;
		private int width = 12;
		private int height = 12;

		public void paintIcon(Component c, Graphics g, int x, int y) {
			Graphics2D g2d = (Graphics2D) g.create();

			g2d.setColor(color);
			switch(form){
			case("Kreis"):
				g2d.fillOval(x +1 ,y + 1,width -2 ,height -2);
			break;
			case("Rechteck"):
				g2d.fillRect(x +1 ,y + 1,width -2 ,height -2);
				break;
			case("Punkt"):
				g2d.drawLine(0, (int)height/2, width, (int)height/2);
				g2d.drawLine((int)width/2, 0, (int)width/2, height);
				break;
			case("Linie"):
				g2d.drawLine(1, height-2, width-2, 1);
				break;
			}

			g2d.dispose();
		}

		@Override
		public int getIconHeight(){ return this.height; }
		@Override
		public int getIconWidth(){ return this.width; }
	}
	
	public void drawto(Graphics g, int zoom){
		if(label == null) return;
		label.drawTo(g, zoom);
	}
	@Override
	public byte[] toByteArray() {
		ArrayList<Byte> tmp = new ArrayList<Byte>();
		
		tmp.add((byte)(amount >> 24));
		tmp.add((byte)(amount >> 16));
		tmp.add((byte)(amount >> 8));
		tmp.add((byte) amount);

		tmp.add((byte)(color.getRGB() >> 24));
		tmp.add((byte)(color.getRGB() >> 16));
		tmp.add((byte)(color.getRGB() >> 8));
		tmp.add((byte) color.getRGB());
		
		byte[] a = name.getBytes();
		for(int i=0; i < a.length;i++){
			tmp.add(a[i]);
		}

		a = form.getBytes();
		for(int i=0; i < a.length;i++){
			tmp.add(a[i]);
		}
		
		int[] b = label.getPos();
		for(int i=0; i < b.length;i++){
			tmp.add((byte)(b[i] >> 24));
			tmp.add((byte)(b[i] >> 16));
			tmp.add((byte)(b[i] >> 8));
			tmp.add((byte) b[i]);
		}

		a = project.getBytes();
		for(int i=0; i < a.length;i++){
			tmp.add(a[i]);
		}
		
		byte[] data = new byte[tmp.size()];
		for(int i=0;i < data.length;i++){
			data[i] = tmp.get(i);
		}
		
		return data;
	}
}
