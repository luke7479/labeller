package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.NumberFormatter;

import labeller.ImageLink;
import labeller.Label;
import labeller.Option;
import labeller.Project;
import server.Serverdata;

public class WizardProject extends JDialog{
	private static final long serialVersionUID = 1L;
	private Container pane;
	private File dir;
	private JButton btnOK;
	private JLabel error;
	private JTextField txtName;
	private JList<Label> labels;
	private DefaultListModel<Label> listModel;
	private JTextField txtFolder;
	private File imgDir;
	private Serverdata server;
	private Option opt;
	private Project project = new Project();
	private JDialog dialog;

	public WizardProject(JFrame parent, Serverdata server, Option opt){
		super(parent,"Project Wizard",Dialog.ModalityType.DOCUMENT_MODAL);

		this.dialog = this;
		this.server = server;
		this.opt = opt;
		
		dir = new File("projects/");
		if(!dir.exists() || !dir.isDirectory()) dir.mkdirs();

		setPreferredSize(new Dimension(400,380));
		setTitle("Neues Projekt anlegen");
		pane = getContentPane();
		setLayout( new BoxLayout( pane, BoxLayout.Y_AXIS ) );
		pack();

		addContent();

		setLocationRelativeTo(parent);
		setVisible(true);
	}

	private void check(){
		//TODO: Icon in Fehlermeldung einf�gen
		File file = new File(dir.getName()+"/"+txtName+".lprj");
		if(txtName.getText().isEmpty() || txtName.getText().length() < 3){
			error.setText("Kein Projektname angegeben");
		}else if(file.exists()){
			error.setText("Projekt existiert bereits!");
		}else if(listModel.size() < 1){
			error.setText("Keine Labels vorhanden");
		}else if(txtFolder.getText().isEmpty() || !imgDir.exists()){
			imgDir = null;
			txtFolder.setText("");
			error.setText("Keine Bilder ausgew�hlt");
		}else{
			error.setText(null);
		}

		btnOK.setEnabled(error.getText() == null);
	}

	private void addContent() {
		addArea();
		addError();
		pane.add(Box.createRigidArea(new Dimension(0,15)));
		addTextField();
		this.add(Box.createRigidArea(new Dimension(0,15)));
		addLabels();
		this.add(Box.createRigidArea(new Dimension(0,15)));
		addFolder();
		this.add(Box.createRigidArea(new Dimension(0,15)));
		addButtons();

		repaint();
	}

	private void addFolder() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.add(new JLabel("Ordner"));
		
		txtFolder = new JTextField();
		panel.add(txtFolder);
		
		JButton btnFolder = new JButton(new ImageIcon(this.getClass().getResource("/folder.png")));
		btnFolder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					JFileChooser chooser = new JFileChooser(new File(new File(".").getAbsolutePath()).getCanonicalPath()+"/projects");
					chooser.setDialogTitle("Bildordner ausw�hlen");
					chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					chooser.setAcceptAllFileFilterUsed(false);
					if(chooser.showOpenDialog(chooser) == JFileChooser.APPROVE_OPTION){
						imgDir = chooser.getSelectedFile();
						txtFolder.setText(imgDir.getCanonicalPath());
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
//					e1.printStackTrace();
				}
				check();
			}
		});
		panel.add(btnFolder);
		
		pane.add(panel);
	}

	private void addButtons() {
		JPanel panel4 = new JPanel();
		panel4.setLayout(new FlowLayout(FlowLayout.RIGHT));
		JButton btnCancel = new JButton("Abbrechen");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				WizardProject.this.dispatchEvent(
					new WindowEvent(WizardProject.this, WindowEvent.WINDOW_CLOSING)
				);
			}
		});
		panel4.add(btnCancel);
		btnOK = new JButton("Fertig");
		btnOK.setEnabled(false);
		btnOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
		panel4.add(btnOK);
		pane.add(panel4);
	}
	
	private void save(){
		String[] extensions = new String[]{"png","jpg","jpeg","bmp","gif"};
		try {
			project.setAuthor(opt.get("username"));
			project.setName(txtName.getText());
						
			for(int i=0;i < listModel.size();i++){
				project.addLabeltoList(listModel.getElementAt(i));
			}
			
			Files.walk(Paths.get(txtFolder.getText())).forEach(filePath -> {
				String ext = filePath.toString().substring(filePath.toString().lastIndexOf('.')+1);
			    if (Files.isRegularFile(filePath) && Arrays.asList(extensions).contains(ext)) {
			    	System.out.println(filePath.toString());
			    	ImageLink imageLink = new ImageLink(filePath.toString());
			    	imageLink.setProject(project.fullName());
			        project.addImage(new ImageLink(imageLink));
			        System.out.println(project.getImageCount());
			        server.addUpload(imageLink);
			    }
			});
			
			project.save();
			server.addUpload(project);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dispose();
	}

	private void addLabels() {
		JPanel panel3 = new JPanel();
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));
		panel3.add(new JLabel("Labels"));
		
		
		listModel = new DefaultListModel<Label>();
		

		labels = new JList<Label>(listModel);
		ListCellRenderer<Label> renderer = new ListEntryCellRenderer();
		labels.setCellRenderer(renderer);
		JScrollPane scrollPane = new JScrollPane(labels);
		panel3.add(scrollPane);

		JPanel panel3_btn = new JPanel();
		panel3_btn.setLayout(new BoxLayout(panel3_btn, BoxLayout.Y_AXIS));
		JButton btnAdd = new JButton(new ImageIcon(this.getClass().getResource("/add.png")));
		btnAdd.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				//listModel.addElement(new Label("Test"+listModel.size(),listModel.size(),Color.BLUE));
				new LabelDialog(dialog);
				check();
			}
		});
		panel3_btn.add(btnAdd);
		JButton btnRem = new JButton(new ImageIcon(this.getClass().getResource("/delete.png")));
		btnRem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(labels.getSelectedIndex() < 0) return;
				
				listModel.remove(labels.getSelectedIndex());
				check();
			}
		});
		panel3_btn.add(btnRem);
		panel3.add(panel3_btn);
		this.add(panel3);
	}

	private void addTextField() {
		//Projektname
		JPanel panel2 = new JPanel();
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));

		JLabel label2 = new JLabel("Projektname: ");
		
		txtName = new JTextField();
		txtName.setToolTipText("G�ltige Zeichen: A-Z,a-z,0-9,-_");
		txtName.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if(!(Character.isLetter(c) || Character.isDigit(c) || c == '-' || c == '_')
						|| c == '�' || c == '�' || c == '�' || c == '�' || c == '�' || c == '�' || c == '�'){
					KeyEvent ke = new KeyEvent(e.getComponent(), KeyEvent.KEY_PRESSED,
							System.currentTimeMillis(), InputEvent.CTRL_MASK,
							KeyEvent.VK_F1, KeyEvent.CHAR_UNDEFINED);
					e.getComponent().dispatchEvent(ke);
					e.consume();
					check();
				}else{
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {}
			
			@Override
			public void keyPressed(KeyEvent e) {}
		});
		txtName.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
//				check();
			}
			public void removeUpdate(DocumentEvent e) {
				check();
			}
			public void insertUpdate(DocumentEvent e) {
//				check();
			}
		});

		panel2.add(label2);
		panel2.add(txtName);
		pane.add(panel2);
	}

	private void addArea() {
		JPanel area = new JPanel();
		area.setLayout(new FlowLayout(FlowLayout.LEFT));
		area.setPreferredSize(new Dimension(this.getWidth(),20));
		area.setBackground(Color.WHITE);


		JLabel label = new JLabel("Ein neues Projekt erstellen");
		area.add(label);

		pane.add(area);
	}

	private JLabel addError() {
		JPanel area = new JPanel();
		area.setLayout(new FlowLayout(FlowLayout.LEFT));
		area.setPreferredSize(new Dimension(this.getWidth(),40));
		area.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
		area.setBackground(Color.WHITE);

		error = new JLabel("");
		area.add(error);

		pane.add(area);

		return error;
	}

	public class LabelListModel extends DefaultListModel<Label>{
		private static final long serialVersionUID = 1L;
		public Label labelAt(int n){
			return (Label) super.getElementAt(n);
		}
		public String labelString(int n){
			Label label = (Label) super.getElementAt(n);
			return label.toString();
		}
		public void addLabel(Label label){
			this.addElement(label);
		}
	}

	class ListEntryCellRenderer extends JLabel implements ListCellRenderer<Label>{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

		@Override
		public Component getListCellRendererComponent(JList<? extends Label> list, Label value, int index, boolean isSelected, boolean cellHasFocus){
			Label entry = (Label) value;

			JLabel renderer = (JLabel)defaultRenderer.getListCellRendererComponent(list,value,index,isSelected,cellHasFocus);

			renderer.setText(entry.toString());
			renderer.setIcon(entry.getIcon());

			if(isSelected){
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			}else{
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}

			setEnabled(list.isEnabled());
			setFont(list.getFont());
			setOpaque(true);

			return renderer;
		}
	}
	
	public class LabelDialog extends JDialog{
		private static final long serialVersionUID = -3447690618923914949L;
		Label label = null;
		GridBagConstraints gbc;
		JDialog dlgLabel;

		public LabelDialog(JDialog parent){
			super(parent,"Neues Label erstellen",true);
			
			dlgLabel = this;
			
			dlgLabel.setLayout(new GridBagLayout());
			gbc = new GridBagConstraints();
			
			addContent();
			
//			dlgLabel.setPreferredSize(new Dimension(300,200));
			dlgLabel.setSize(new Dimension(300,150));
			dlgLabel.setLocationRelativeTo(parent);
//			setResizable(false);
			dlgLabel.setVisible(true);
		}
		
		private void addContent(){
			addGrid(new JLabel("Name: "),1,1,1,1,1.0,1.0);
			JTextField txtName = new JTextField(); 
			addGrid(txtName,2,1,1,1,1.0,1.0);
			
			addGrid(new JLabel("Anzahl: "),1,2,1,1,1.0,1.0);
			NumberFormat format = NumberFormat.getInstance();
	        format.setGroupingUsed(false); 
	        NumberFormatter formatter = new NumberFormatter(format); 
	        formatter.setAllowsInvalid(false); 
			JFormattedTextField txtCount =  new JFormattedTextField(formatter);
			addGrid(txtCount,2,2,1,1,1.0,1.0);

			addGrid(new JLabel("Farbe: "),1,3,1,1,1.0,1.0);
			JButton color = new JButton("    ");
			color.setBackground(Color.BLACK);
			color.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Color newColor = JColorChooser.showDialog( null,"W�hle neue Farbe",color.getBackground() );
					if(newColor == null) return;
					color.setBackground( newColor );
				}
			});
			addGrid(color,2,3,1,1,1.0,1.0);

			addGrid(new JLabel("Form: "),1,4,1,1,1.0,1.0);
			JComboBox<String> form = new JComboBox<String>(new String[]{"Punkt","Linie","Kreis","Rechteck"});
			addGrid(form,2,4,1,1,1.0,1.0);

			JButton btnCancel = new JButton("Abbrechen");
			btnCancel.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					dlgLabel.dispose();
				}
			});
			addGrid(btnCancel,1,5,1,1,1.0,1.0);
			JButton btnOK = new JButton("Ok");
			btnOK.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int count = (Integer.parseInt(txtCount.getText()) < 1)? 1 : Integer.parseInt(txtCount.getText());
					label = new Label(txtName.getText(),count,color.getBackground(),form.getItemAt(form.getSelectedIndex()));
					listModel.addElement(label);
					dlgLabel.dispose();
				}
			});
			addGrid(btnOK,2,5,1,1,1.0,1.0);
		}
		
		private void addGrid(JComponent comp,int x, int y, int width, int height, double weightX, double weightY){
			gbc.gridx = x;
			gbc.gridy = y;
			gbc.weightx = weightX;
			gbc.weighty = weightY;
			gbc.gridwidth = width;
			gbc.gridheight = height;
			if(!(comp instanceof JButton)){
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.anchor = GridBagConstraints.NORTHWEST;
			}
			add(comp, gbc);
		}
	}
}
