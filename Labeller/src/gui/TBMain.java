package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import labeller.Registry;

public class TBMain extends ToolBar{
	private static final long serialVersionUID = 5275444809223026867L;

	public TBMain(Actions actions, Registry reg){
		this.reg = reg;
		this.actions = actions;
		addElements();
		addSeparator(new Dimension(8,20));
	}
	
	private void addElements(){
		JButton btn;
		
		btn = new JButton(new ImageIcon(this.getClass().getResource(reg.resources+"load.png")));
			btn.setToolTipText("�ffnen");
			buttons.put(btn, null);
			btn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					actions.load();
				}
			});
			this.add(btn);
		btn = new JButton(new ImageIcon(this.getClass().getResource(reg.resources+"save.png")));
			btn.setToolTipText("Speichern");
			buttons.put(btn, "modified");
			btn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					actions.save();
				}
			});
			this.add(btn);
		btn = new JButton(new ImageIcon(this.getClass().getResource(reg.resources+"close.png")));
			btn.setToolTipText("Schlie�en");
			buttons.put(btn, "project");
			btn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					actions.closeProject();
				}
			});
			this.add(btn);
	}
}
