package labeller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import server.Serverdata;
import server.Uploadable;

public class Project implements Uploadable, Serializable{
	private static final long serialVersionUID = -9157088706748905971L;
	private File file = null;
	private String name = null;
	private LinkedList<Label> labelList = new LinkedList<Label>();
	private LinkedList<ImageLink> images= new LinkedList<ImageLink>();
	private String author = "";
	private Date created = new Date();
	private int version = 3;
	private boolean loaded = false; 
	private boolean uploaded = false;
	private HashMap<Integer, List<Label>> labels = new HashMap<Integer, List<Label>>();
	private int currentImage = 0;
	
	public Project(){}
	
	public Project(String name){
		setName(name);
	}
	
	public Project(File file){
		this.file = file;
		load();
	}
	
	public String getAuthor(){
		return author;
	}
	public void setAuthor(String author){
		if(author.isEmpty()) author = null;
		File tmp = null;
		if(name != null && author != null) tmp = new File("projects/"+author+"_"+name+".lprj");
		this.file = tmp;
		this.author = author;
	}
	
	public int getCurrentImage(){
		return currentImage;
	}
	public void setCurrentImage(int image){
		if(image < 0 || image >= images.size()) return;
		currentImage = image;
	}
	
	public String getFile() {
		return file.getPath();
	}
	public String getFilename(){
		return file.getName();
	}
	public void setName(String name){
		if(name.isEmpty()) name = null;
		File tmp = null;
		if(name != null && author != null) tmp = new File("projects/"+author+"_"+name+".lprj");
		this.file = tmp;
		this.name = name;
	}
	
	public void addImage(ImageLink image){
		images.add(image);
	}
	public int getImageCount(){
		return images.size();
	}
	public LinkedList<ImageLink> getImages(){
		return images;
	}
	public String getName() {
		return name;
	}
	public String getPath(){
		return author+"_"+name;
	}
	public void setImages(LinkedList<ImageLink> images){
		if(images == null) return;
		this.images = images;
	}
	
	public void addLabel(Label label){
		getLabels().add(label);
	}
	public int getLabelCount(int image){
		return getLabels().size();
	}
	public LinkedList<Label> getLabels(){
		if(labels.get(currentImage) == null) labels.put(currentImage, new LinkedList<Label>());
		return (LinkedList<Label>) labels.get(currentImage);
	}
	public LinkedList<Label> getLabelList(){
		return labelList;
	}
	
	public boolean getUploaded(){
		return uploaded;
	}
	public void setUploaded(boolean status){
		uploaded = status;
	}
	
	@SuppressWarnings("unchecked")
	private ObjectInputStream loadStream(ObjectInputStream in){
		if(in == null) return in;
		try {
			int fileVersion = (int)in.readObject();
			if(fileVersion >= 1){
				author = (String)in.readObject();
				created = (Date)in.readObject();
				name = (String)in.readObject();
				labelList = (LinkedList<Label>)in.readObject();
				labels = (HashMap<Integer, List<Label>>)in.readObject();
				images = (LinkedList<ImageLink>)in.readObject();
			}
			if(fileVersion >= 2){
				uploaded = (boolean)in.readObject();
			}
			if(fileVersion >= 3){
				currentImage = (int)in.readObject();
			}
		} catch (ClassNotFoundException | IOException e) {
		}
		return in;
	}
	
	private boolean load(){
		if(file == null || !file.exists() || !file.canRead()) return false;
		FileInputStream fis=null;
		ObjectInputStream in=null;
		try {
			
			fis = new FileInputStream(file);
			in = new ObjectInputStream(fis);
			
			loadStream(in);
			
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally{
			try {
				fis.close();
				in.close();
			} catch (Exception e1) {}
		}
		
		if(!Option.getInstance().get("username").equals(author)){
			File dir = new File("projects/"+getPath());
			if(!dir.exists()){
				dir.mkdirs();
			}
			
			Serverdata server = Serverdata.getInstance();
			for(int i=0;i < images.size();i++){
				ImageLink load = new ImageLink(images.get(i));
				load.setProject(fullName());
				server.addDownload(load);
			}
		}
		loaded = true;
		return true;
	}
	
	public ObjectOutputStream saveStream(OutputStream os){
		ObjectOutputStream out=null;
		try {
			out = new ObjectOutputStream(os);
			out.writeObject(version);
			out.writeObject(author);
			out.writeObject(created);
			out.writeObject(name);
			out.writeObject(labelList);
			out.writeObject(labels);
			out.writeObject(images);
			out.writeObject(uploaded);
			out.writeObject(currentImage);
		} catch (IOException e) {
			return null;
		}
		return out;
	}
	
	public boolean save(){
		if(author == null || file == null || name == null) return false;
		FileOutputStream fos=null;
		ObjectOutputStream out=null;
		try {
			fos = new FileOutputStream(file);
			out = saveStream(fos);
		} catch (IOException e) {
			return false;
		} finally{
			try {
				fos.close();
				out.close();
			} catch (Exception e1) {}
		}
		return true;
	}

	public boolean loaded() {
		return loaded;
	}

	public ImageLink getImage() {
		return images.get(currentImage);
	}

	public void addLabeltoList(Label label) {
		labelList.add(label);
	}

	@Override
	public byte[] toByteArray() {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] data = new byte[]{};;
	  saveStream(bos);
	  data = bos.toByteArray();
	  try {
	    if(bos != null) bos.close();
	  } catch (IOException ex) {}
		
		return data;
	}

	public void deletePaths() {
		for(int i=0;i < images.size();i++){
			ImageLink image = images.get(i);
			if(image.pathCleaned()) continue;
			File file = new File(image.toString());
			if(!file.exists() || !file.canRead()){
				images.remove(image);
				continue;
			}
			image.deletePath(author+"_"+name);
		}
	}

	public String fullName() {
		return author+"_"+name;
	}
}
