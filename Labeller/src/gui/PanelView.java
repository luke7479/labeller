package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import javax.swing.JPanel;

import server.Serverdata;
import labeller.ImageLoader;
import labeller.Label;
import labeller.Position;
import labeller.Registry;
import labeller.Viewport;

public class PanelView  extends JPanel{
	private static final long serialVersionUID = 2924456615397502338L;
	private Position mouse = new Position(0,0);
	private BufferedImage buffer;
	private ImageLoader loader;
	private Viewport view = new Viewport(0, 0, 0, 0, 0.0, 0.0);
	private BufferedImage image;
	private BufferedImage resized;
	private int zoom=-1;
	private Registry reg;
	private Gui frame;
	private Drawable temp = null;
	private Serverdata server;
	
	public PanelView(ImageLoader loader, Gui frame, Actions actions, Registry reg, Serverdata server) {
		this.loader = loader;
		this.reg = reg;
		this.frame = frame;
		this.server = server;
		
		ActionsView listener = new ActionsView(actions, frame, this, reg);
		addMouseListener(listener);
		addMouseWheelListener(listener);
		addMouseMotionListener(listener);
		actions.setPanelView(this);
	}

	public void setMouseXY(Position pos) {
		mouse = pos;
		frame.repaintImage(true, false);
	}

	public void nextImage() {
		if(reg.project == null) return;
		int current = reg.project.getCurrentImage();
		reg.project.setCurrentImage(current+1);
		showImage(reg.project.getCurrentImage());
	}

	public void prevImage() {
		if(reg.project == null) return;
		int current = reg.project.getCurrentImage();
		reg.project.setCurrentImage(current-1);
		showImage(reg.project.getCurrentImage());
	}

	public void showImage(int i) {
		image = loader.getImage();
		resized = null;
		frame.repaintImage(true, false);
	}
	
	@Override
	public void paintComponent(Graphics g){
		if(reg.project == null){
			image = null;
			resized = null;
		}
		if(buffer == null || buffer.getWidth() != getWidth() || buffer.getHeight() != getHeight()){
			buffer = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		}
		drawImage(buffer.createGraphics());
		paintLabel(buffer.createGraphics());
		g.drawImage(buffer, 0, 0, this);
		
		if(resized != null && !hasFocus()) grabFocus();
		
		loader.setView(view);
		frame.repaintImage(false, true);
	}
	
	private void drawImage(Graphics g){
		g.setColor(new Color(80,80,80));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(reg.project == null) return;

		if(resizeImage(image)){
			drawFinishedLabels(resized.createGraphics());
			zoom = reg.zoom;
		}
		if(resized != null){
			int imageX = getWidth()/2-resized.getWidth()/2+view.X;
			int imageY = getHeight()/2-resized.getHeight()/2+view.Y;
			moveImage(0,0,false);
			g.drawImage(resized, imageX, imageY, this);
		}
		
		if(mouse.X >= 0 && mouse.Y >= 0){
			g.setColor(new Color(180,180,180));
			g.drawLine(0, mouse.Y, getWidth(), mouse.Y);
			g.drawLine(mouse.X, 0, mouse.X, getHeight());
		}
	}

	private boolean resizeImage(BufferedImage image) {
		if(reg.zoom == zoom && resized != null) return false;

		double factor = reg.zoom/100.0;
		int width = (int)(image.getWidth()*factor);
		int height = (int)(image.getHeight()*factor);
		
		
		resized = loader.resizeImage(image,width,height);
		
		view.IMAGE_X = resized.getWidth();
		view.IMAGE_Y = resized.getHeight();
		view.WIDTH = getWidth();
		view.HEIGHT = getHeight();
		view.RATIO_X = (double)resized.getWidth()/getWidth();
		view.RATIO_Y = (double)resized.getHeight()/getHeight();
		
		
		return true;
	}

	public void scrollVertical(int scrollWidth) {
		moveImage(0,scrollWidth);
	}

	public void scrollHorizontal(int scrollWidth) {
		moveImage(scrollWidth,0);
	}

	public void moveImage(int x, int y){
		moveImage(x, y, true);
	}
	public void moveImage(int x, int y, boolean repaint) {
		if(resized == null) return;
		if(resized.getWidth() > getWidth()){
			view.X-=x;
			if(view.X > 0 && resized.getWidth()-getWidth()/2-view.X < resized.getWidth()/2) view.X = resized.getWidth()/2-getWidth()/2;
			if(view.X < 0 && Math.abs(getWidth()/2-resized.getWidth()-view.X) < resized.getWidth()/2) view.X = getWidth()/2-resized.getWidth()/2;
		}else{
			view.X = 0;
		}
		if(resized.getHeight() > getHeight()){
			view.Y-=y;
			if(view.Y > 0 && resized.getHeight()-getHeight()/2-view.Y < resized.getHeight()/2) view.Y = resized.getHeight()/2-getHeight()/2;
			if(view.Y < 0 && Math.abs(getHeight()/2-resized.getHeight()-view.Y) < resized.getHeight()/2) view.Y = getHeight()/2-resized.getHeight()/2;
		}else{
			view.Y = 0;
		}
		if(repaint) frame.repaintImage(true, true);
	}
	
	public void drawFinishedLabels(Graphics g){
		LinkedList<Label> labels = reg.project.getLabels();
		if(labels == null) return;
		for(Label label: labels){
			label.drawto(g, reg.zoom);
		}
	}

	public void drawLabel(final int[] labelPos) {
		if(reg.project == null) return;
		int[] position = new int[]{
				Math.min(labelPos[0], labelPos[2]), 
				Math.min(labelPos[1], labelPos[3]),
				Math.max(labelPos[0], labelPos[2]), 
				Math.max(labelPos[1], labelPos[3])
		};
		
		int label = reg.labelIndex;
		Label currentLabel = reg.project.getLabelList().get(label);
		if(currentLabel.getForm().equals("Kreis")){
			if(temp != null && temp instanceof DrawCircle){
				((DrawCircle)temp).setX(position[0]);
				((DrawCircle)temp).setY(position[1]);
				((DrawCircle)temp).setWidth(position[2]-position[0]);
				((DrawCircle)temp).setHeight(position[3]-position[1]);
			}else{
				temp = new DrawCircle(position[0], position[1], position[2]-position[0], position[3]-position[1], currentLabel.getColor());
			}
		}else if(currentLabel.getForm().equals("Linie")){
			if(temp != null && temp instanceof DrawLine){
				((DrawLine)temp).setX1(labelPos[0]);
				((DrawLine)temp).setY1(labelPos[1]);
				((DrawLine)temp).setX2(labelPos[2]);
				((DrawLine)temp).setY2(labelPos[3]);
			}else{
				temp = new DrawLine(position[0], position[1], position[2], position[3], currentLabel.getColor());
			}
		}else if(currentLabel.getForm().equals("Punkt")){
			if(temp != null && temp instanceof DrawCross){
				((DrawCross)temp).setX(labelPos[2]);
				((DrawCross)temp).setY(labelPos[3]);
			}else{
				temp = new DrawCross(labelPos[2], labelPos[3], currentLabel.getColor());
			}
		}else if(currentLabel.getForm().equals("Rechteck")){
			if(temp != null && temp instanceof DrawRect){
				((DrawRect)temp).setX(position[0]);
				((DrawRect)temp).setY(position[1]);
				((DrawRect)temp).setWidth(position[2]-position[0]);
				((DrawRect)temp).setHeight(position[3]-position[1]);
			}else{
				temp = new DrawRect(position[0], position[1], position[2]-position[0], position[3]-position[1], currentLabel.getColor());
			}
		}
	}
	
	public void finishLabel(){
		if(temp == null) return;

		Label currentLabel = reg.project.getLabelList().get(reg.labelIndex); 
		Label newLabel = new Label(currentLabel);
		newLabel.setProject(reg.project.getName());
		temp.posToAbs(view,this, reg.zoom, resized);
		newLabel.setLabel(temp);
		reg.project.addLabel(newLabel);
		newLabel.drawto(resized.createGraphics(), reg.zoom);
		server.addUpload(newLabel);
		reg.project.save();
		temp=null;
	}
	
	private void paintLabel(Graphics g){
		if(temp == null) return;
		temp.drawTo(g);
	}
}
