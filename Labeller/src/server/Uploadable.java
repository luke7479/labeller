package server;

import java.io.Serializable;



public interface Uploadable extends Serializable {
	public byte[] toByteArray();
}
