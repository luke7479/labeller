package gui;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import labeller.Viewport;

/**
 * Class to draw circles into graphics
 * @author Sascha Ha�ler
 */
public class DrawCircle extends Drawable{
	private static final long serialVersionUID = -575771711843453564L;
	private int x;
	private int y;
	private int width;
	private int height;
	private Color color;
	
	public DrawCircle(int x, int y, int width, int height){ this(x,y,width,height,Color.BLACK); }
	public DrawCircle(int x, int y, int width, int height, Color color){
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);
		setColor(color);
	}
	
	/**
	 * Method to set line-/fill-color
	 * @param color <b>[Color]</b> {@link java.awt.Color}
	 */
	public void setColor(Color color){
		this.color = color;
	}
	
	public void setX(int x){ this.x = x; }
	public void setY(int y){ this.y = y; }
	public void setWidth(int width){ this.width = width; }
	public void setHeight(int height){ this.height = height; }
	
	public int getX(){ return this.x; }
	public int getY(){ return this.y; }
	public int getWidth(){ return this.width; }
	public int getHeight(){ return this.height; }
	public int[] getPos(){ return new int[]{x,y,width,height}; }
	
	/**
	 * Returns parameters to String
	 */
	public String toString(){
		return "Circle: "+x+","+y+","+width+","+height;
	}
	
	@Override
	public void drawTo(Graphics g){
		drawTo(g, 100);
	}

	@Override
	public void drawTo(Graphics g, int zoom){
		int x = (int)(this.x*zoom/100.0);
		int y = (int)(this.y*zoom/100.0);
		int width = (int)(this.width*zoom/100.0);
		int height = (int)(this.height*zoom/100.0);
		
		float trans = 0.5f;
		Graphics2D g2d = (Graphics2D)g;
		Composite original = g2d.getComposite();
		Composite translucent = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, trans);
		Composite translucent2 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, trans*1.5f);
		
		g2d.setComposite(translucent);
		g2d.setColor(this.color);
		g2d.fillOval(x, y, width,height);

		g2d.setComposite(translucent2);
		g2d.drawOval(x,y,width,height);
		g2d.drawOval(x+1,y+1,width-2,height-2);
		
		g2d.setComposite(original);
	}
	@Override
	public void posToAbs(Viewport view, PanelView panelView, int zoom, BufferedImage resized) {
		x = Math.round((resized.getWidth() /2-view.X-panelView.getWidth() /2+x)*100.f/zoom);
		y = Math.round((resized.getHeight()/2-view.Y-panelView.getHeight()/2+y)*100.f/zoom);
		width =  Math.round(width *100.f/zoom);
		height = Math.round(height*100.f/zoom);
	}
}
