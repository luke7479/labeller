package server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import labeller.ImageLink;
import labeller.Option;

public class ThreadDownload implements Runnable {
	private Serverdata data;
	private Option opt = Option.getInstance();
	
	public ThreadDownload(Serverdata data){
		this.data = data;
	}

	@Override
	public void run() {
		Thread.currentThread().setName("Download");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {}
		
		while(true){
			if(!Server.online() || data.getDownloads() < 1){
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {}
				continue;
			}
			
			Downloadable load = data.getDownload();
			if(load == null) continue;
			download(load, 1);
		}
	}
	
	private void download(Downloadable load, int i) {
		if(!(load instanceof ImageLink)) return;
		File file = new File(((ImageLink)load).toString());
		
		while(!Server.checkFile((ImageLink)load)) getFile(file, ((ImageLink)load).getProject());
		data.removeDownload();
	}

	public void start(){
		this.start();
	}

	private void getFile(File file, String project){
		FileOutputStream fos = null;
		if(file.exists()) file.delete();
		
		try {
			String link = Server.getLink()+"?action=getImage&username="+opt.get("username")+"&password="+opt.get("password")+"&filename="+file.getName()+"&project="+project;
			System.out.println(link);
			URL website = new URL(link);
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			fos = new FileOutputStream("projects/"+project+"/"+file.getName());
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		} catch (IOException e) {
		} finally {
			try {
				if(fos != null) fos.close();
			} catch (IOException e) {}
		}
	}
}
