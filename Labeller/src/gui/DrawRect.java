package gui;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import labeller.Viewport;


/**
 * Class to draw rectangles into graphics
 * @author Sascha Ha�ler
 * @version 1.0
 *
 */
public class DrawRect extends Drawable{
	private static final long serialVersionUID = 4217189218595889650L;
	private int x;
	private int y;
	private int w;
	private int h;
	private Color color;

	/**
	 * Constructor and setter for:
	 * @param x <b>[int]</b> X-Position of top-left corner
	 * @param y <b>[int]</b> Y-Position of top-left corner
	 * @param w <b>[int]</b> Width from top-left to top-right corner
	 * @param h <b>[int]</b> Height from top-left to bottom-left corner
	 */
	public DrawRect(int x, int y, int w, int h){this(x,y,w,h,Color.BLACK);}
	public DrawRect(int x, int y, int w, int h, Color color){
		this.x=x;
		this.y=y;
		this.w=w;
		this.h=h;
		this.color = color;
	}
	
	public void setX(int x){ this.x = x; }
	public void setY(int y){ this.y = y; }
	public void setWidth(int width){ this.w = width; }
	public void setHeight(int height){ this.h = height; }
	
	public int getX(){ return this.x; }
	public int getY(){ return this.y; }
	public int getWidth(){ return this.w; }
	public int getHeight(){ return this.h; }
	public int[] getPos(){ return new int[]{x,y,w,h}; }
	
	/**
	 * Method to set line-/fill-color
	 * @param col <b>[Color]</b> {@link java.awt.Color}
	 */
	public void setColor(Color color){
		this.color = color;
	}
	
	/**
	 * Returns parameters to String
	 */
	public String toString(){
		return "Rectangle: "+x+","+y+","+w+","+h;
	}
	
	@Override
	public void drawTo(Graphics g){
		drawTo(g, 100);
	}

	@Override
	public void drawTo(Graphics g, int zoom){
		int x = (int)(this.x*zoom/100.0);
		int y = (int)(this.y*zoom/100.0);
		int w = (int)(this.w*zoom/100.0);
		int h = (int)(this.h*zoom/100.0);
		
		float trans = 0.5f;
		Graphics2D g2d = (Graphics2D)g;
		Composite original = g2d.getComposite();
		Composite translucent = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, trans);
		Composite translucent2 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, trans*1.5f);
		
		g2d.setColor(this.color);
		g2d.setComposite(translucent);
		g.fillRect(x, y, w, h);

		g2d.setComposite(translucent2);
		g.drawRect(x, y, w, h);
		g.drawRect(x+1, y+1, w-2, h-2);
		
		g2d.setComposite(original);
	}
	@Override
	public void posToAbs(Viewport view, PanelView panelView, int zoom, BufferedImage resized) {
		x = Math.round((resized.getWidth() /2-view.X-panelView.getWidth() /2+x)*100.f/zoom);
		y = Math.round((resized.getHeight()/2-view.Y-panelView.getHeight()/2+y)*100.f/zoom);
		w = Math.round(w*100.f/zoom);
		h = Math.round(h*100.f/zoom);
	}
}
