package labeller;


public class Registry {
	public int labelIndex			= 0;
	public boolean modified 		= false;
	public Project project			= null;
	
	public int zoom					= 100;
	public boolean undo				= false;
	
	public boolean online			= false;
	public String version			= "0.53";
	public String resources			= "/";
}
