package gui;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import labeller.Viewport;


/**
 * Class to draw lines into graphics
 * @author Sascha Ha�ler
 * @version 1.0
 *
 */
public class DrawLine extends Drawable{
	private static final long serialVersionUID = 5880223392608575471L;
	private int x1;
	private int y1;
	private int x2;
	private int y2;
	private Color color;

	/**
	 * Constructor and setter for:
	 * @param x1 <b>[int]</b> X-position to start
	 * @param y1 <b>[int]</b> Y-position to start
	 * @param x2 <b>[int]</b> X-position to end
	 * @param y2 <b>[int]</b> Y-position to end
	 */
	public DrawLine(int x1, int y1, int x2, int y2){this(x1,y1,x2,y2,Color.BLACK);}
	public DrawLine(int x1, int y1, int x2, int y2, Color color){
		this.x1=x1;
		this.x2=x2;
		this.y1=y1;
		this.y2=y2;
		this.color = new Color(0x99FFFFFF & color.getRGB());
	}
	
	public void setX1(int x1){ this.x1 = x1; }
	public void setY1(int y1){ this.y1 = y1; }
	public void setX2(int x2){ this.x2 = x2; }
	public void setY2(int y2){ this.y2 = y2; }
	
	public int getX1(){ return this.x1; }
	public int getY1(){ return this.y1; }
	public int getX2(){ return this.x2; }
	public int getY2(){ return this.y2; }
	public int[] getPos(){ return new int[]{x1,y1,x2,y2}; }
	
	/**
	 * Method to set line-/fill-color
	 * @param col <b>[Color]</b> {@link java.awt.Color}
	 */
	public void setColor(Color color){
		this.color = new Color(0x99FFFFFF & color.getRGB());
	}	
	/**
	 * Returns parameters to String
	 */
	public String toString(){
		return "Line: "+x1+","+y1+","+x2+","+y2;
	}
	
	@Override
	public void drawTo(Graphics g){
		drawTo(g, 100);
	}

	@Override
	public void drawTo(Graphics g, int zoom){
		int x1 = (int)(this.x1*zoom/100.0);
		int y1 = (int)(this.y1*zoom/100.0);
		int x2 = (int)(this.x2*zoom/100.0);
		int y2 = (int)(this.y2*zoom/100.0);
		
		g.setColor(this.color);
		
		g.drawLine(x1, y1, x2, y2);
	}
	@Override
	public void posToAbs(Viewport view, PanelView panelView, int zoom, BufferedImage resized) {
		x1 = Math.round((resized.getWidth() /2-view.X-panelView.getWidth() /2+x1)*100.f/zoom);
		y1 = Math.round((resized.getHeight()/2-view.Y-panelView.getHeight()/2+y1)*100.f/zoom);
		x2 = Math.round((resized.getWidth() /2-view.X-panelView.getWidth() /2+x2)*100.f/zoom);
		y2 = Math.round((resized.getHeight()/2-view.Y-panelView.getHeight()/2+y2)*100.f/zoom);
	}
}
